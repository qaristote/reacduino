INO = ino
INO_FLAGS = 

HEPTC = heptc
HEPTC_FLAGS = -itfusion -memalloc

EPT_FILES := $(shell find src -name *.ept)
EPI_FILES := $(shell find lib -name *.epi)
C_FILES := $(shell find src -name *.c -or -name *.h -or -name *.ino)

ept_to_c_dir = $(dir $(1))$(basename $(notdir $(1)))_c/
EPT_C_DIRS := $(foreach file,$(EPT_FILES),$(call ept_to_c_dir,$(file)))
EPCI_FILES := $(EPI_FILES:.epi=.epci)
HEPTC_INCLUDE_LIBS := $(addprefix -I ,$(dir $(realpath $(EPI_FILES))))

ODOC = ocamldoc
ODOC_FLAGS = -html

LATEX = lualatex
LATEX_FLAGS = --shell-escape
BIBTEX = biber
BIBTEX_FLAGS =

EXAMPLES := $(shell ls examples)
NAME = 


.PHONY : flash ino ept doc report example save clean


flash : ino
	ino upload


ino : ept $(C_FILES)
	$(INO) build $(INO_FLAGS)


ept : $(EPT_C_DIRS)

$(EPT_C_DIRS) : %_c/ : %.ept lib/heptagon/pervasives.h $(EPCI_FILES)
	cd $(dir $<) ; $(HEPTC) $(HEPTC_FLAGS) $(HEPTC_INCLUDE_LIBS) -c $(notdir $<)
	cd $(dir $<) ; $(HEPTC) $(HEPTC_FLAGS) $(HEPTC_INCLUDE_LIBS) -target c $(notdir $<)	

%.epci : %.epi
	cd $(dir $<) ; $(HEPTC) $(HEPTC_FLAGS) $(notdir $<)

lib/heptagon/pervasives.h :
	mkdir -p $(dir $@)
	ln -s $(shell $(HEPTC) -where)/c/$(notdir $@) $(dir $@)

doc : $(EPI_FILES)
#		the .epi file is converted to a .mli file using the following sed rules :
#		1. replace 'fun <foo>' with 'val <foo> : ' and similarly for nodes
# 		2. merge the next line as long as the declaration doesn't contain 'returns'
#		   and end with ')'
#		3. replace '<var> : <type>' with '<type>' in a declaration
#		4. replace '()' with '(unit)' in a declaration
#		5. replace '<type> ; ... ; <type>' with '<type> * ... * <type>' in a declaration
#		6. remove parenthesis and closing angle brackets in a declaration
#		7. replace '>>' with '->' in a declaration
#		8. replace 'returns' with '->' in a declaration
#		9. remove unnecessary whitespace
#		the doc is then generated from the .mli file
	for file in $(EPI_FILES) ; do \
		file_base=$$(basename $$file | cut -f1 -d.) ; \
		file_dir=doc/odoc/$$file_base ; \
		mkdir -p $$file_dir ; \
		cd $$file_dir ; \
		file_mli=$$file_base.mli ; \
		sed \
		-e 's/^[[:space:]]*\(fun\|node\)[[:space:]]\+\([a-Z0-9_]*\)/val \2 : /' \
		-e '/^val [a-Z0-9_]\+ : / {:a ; /.*returns.*)[[:space:]]*$$/! {N ; s/\n// ; ba}}' \
		-e '/^val [a-Z0-9_]\+ : / s/[a-Z0-9_]\+[[:space:]]*:[[:space:]]*\([a-Z0-9_]\+\)/\1/g' \
		-e '/^val [a-Z0-9_]\+ : / s/()/(unit)/g' \
		-e '/^val [a-Z0-9_]\+ : / s/[[:space:]\n]*;[[:space:]\n]*/ \* /g' \
		-e '/^val [a-Z0-9_]\+ : / s/[()<]//g' \
		-e '/^val [a-Z0-9_]\+ : / s/>>/ -> /' \
		-e '/^val [a-Z0-9_]\+ : / s/returns/ -> /' \
		-e '/^val [a-Z0-9_]\+ : / s/[[:space:]]\+/ /g' \
		../../../$$file > $$file_mli ; \
		$(ODOC) $(ODOC_FLAGS) -intf $$file_mli ; \
		cd ../../.. ; \
	done

report : doc/report/report.tex lib/reacduino/reacduino.epi
	$(MAKE) doc ODOC_FLAGS="-latex -o doc.tex"
	sed -n '/\\begin{document}/,$$p' -i doc/odoc/reacduino/doc.tex
	sed \
	-e '/\\tableofcontents/d' \
	-e 's/\([^ ]*\)\[\\url{\([^}]*\)}\]/\\href{\2}{\1}/g' \
	-i doc/odoc/reacduino/doc.tex
	cd doc/report ; \
	$(LATEX) $(LATEX_FLAGS) report.tex ; \
	$(BIBTEX) $(BIBTEX_FLAGS) report ; \
	$(LATEX) $(LATEX_FLAGS) report.tex ; \
	$(LATEX) $(LATEX_FLAGS) report.tex

ifeq "$(NAME)" ""
example :
	@echo "usage : make example NAME=..."
else
example : clean
	rm -rf src lib/local
	cp -r examples/$(NAME)/src src
	cp -r examples/$(NAME)/lib lib/local
endif

ifeq "$(NAME)" ""
save :
	@echo "usage : make save NAME=..."
else	
save : clean
	mkdir -p examples/$(NAME)
	rm -rf examples/$(NAME)/src examples/$(NAME)/lib
	cp -r src/ examples/$(NAME)/src
	cp -r lib/local examples/$(NAME)/lib
endif


clean :
	find src -name *.epci -delete || true
	find src -name *.log  -delete || true
	find src -name *.mls  -delete || true
	find src -name *.epo  -delete || true
	find src -name *.obc  -delete || true
	find src -name *_c -type d -exec rm -r {} + || true
	$(INO) clean || true
	git clean -xdf -e src/ -e lib/local || true
