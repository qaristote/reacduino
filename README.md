# Reacduino

The goal of this repository is to provide examples of how reactive synchronous 
languages, such as [Heptagon](http://heptagon.gforge.inria.fr/), can be used to 
better program microcontrollers, such as [Arduinos](https://www.arduino.cc/). 
The aim is not to fully replace C code with Heptagon code, but only parts of the
code that would be better expressed in a reactive way.

# Requirements

Both [the Heptagon compiler](http://heptagon.gforge.inria.fr/) and 
[the Ino toolkit](http://inotool.org/) are required to build the code. To generate the documentation, [odoc](https://github.com/ocaml/odoc) is required. To build the report, `lualatex` and `biber` are required.

# Building

The `Makefile` allows the use of the following targets :

- `make ept` compiles the Heptagon code to C code ;
- `make ino` builds the binary executable to be transferred to the 
microcontroller ;
- `make flash` builds and transfers the binary executable to the 
microcontroller ;
- `make doc` generates documentation for the heptagon libraries ;
- `make report` generates the development report ;
- `make clean` removes the files generated with by previous commands.

Additionnally, `make example NAME=<dirname>` copies `examples/<dirname>/src` to `src`, so that it can be compiled or built upon, and conversely `make save NAME=<dirname>` copies the content of `src` to `examples/<dirname>/src/`. Use with caution, as those commands may overwrite files.

# Contributing

The code should follow the guidelines of the Ino toolkit (mainly having source
code in `src/` and additional libraries in `lib/`).
