% !Mode:: "TeX:UTF-8"
\documentclass[a4paper,french]{scrartcl}

%%%%%%%%%%%%%%%%%%%% PACKAGES %%%%%%%%%%%%%%%%%%%%
% to write in French
\usepackage[french]{babel}
\RequirePackage{microtype}
\usepackage{lmodern}
%\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{csquotes}
% to write code
\usepackage{caption}
\usepackage[newfloat]{minted}
% to use the generated doc
\usepackage{subfiles}
\usepackage{textcomp}
\usepackage{url}
\usepackage{../../doc/odoc/reacduino/ocamldoc}
% to make references
\RequirePackage{xcolor}
\usepackage[sorting=none]{biblatex}
\addbibresource{biblio.bib}
\usepackage[colorlinks, urlcolor=blue, citecolor=blue,linkcolor=blue]{hyperref}
\RequirePackage[document]{ragged2e}

%%%%%%%%%%%%%%%%%%%% COMMANDS %%%%%%%%%%%%%%%%%%%%
% to write code
\newenvironment{code}{\captionsetup{type=listing}}{}
\SetupFloatingEnvironment{listing}{name=Programme}
\newcommand{\inputexample}[2][text]{\inputminted[linenos]{#1}{../../examples/#2}}
% fix odoc incompatibility with koma
\makeatletter
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\makeatother

%%%%%%%%%%%%%%%%%%%% TITLE %%%%%%%%%%%%%%%%%%%%
\title{Reacduino : une librairie pour la programmation de microcontrôleurs Arduino dans le langage réactif Heptagon}
\author{Quentin Aristote, Clément Ogier}
\date{17 janvier 2021}

%%%%%%%%%%%%%%%%%%%% BODY %%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle

{
	\hypersetup{hidelinks}
	\tableofcontents
}

\section{Introduction}
\label{sec:introduction}

Les cartes Arduino~\cite{Arduino} sont des cartes électroniques équipées de microcontrôleurs programmables destinées au grand public. Une librairie dans le langage C est fournie pour faciliter la programmation de ces microcontrôleurs~\cite{ArduinoReference}. Par exemple, un montage simple~\cite{fitzgeraldBlink} permet de faire clignoter une diode par le biais du \autoref{code:clignotement-diode}.

\begin{code}
  \captionof{listing}{Clignotement d'une diode}
  \label{code:clignotement-diode}
  \begin{minted}[linenos]{arduino}
    void setup() {
      pinMode(LED_BUILTIN, OUTPUT) ;
    }
    
    void loop() {
      digitalWrite(LED_BUILTIN, HIGH) ;
      delay(1000) ;
      digitalWrite(LED_BUILTIN, LOW) ;
      delay(1000) ;
    }
  \end{minted}
\end{code}

La fonction \texttt{setup()} est appelée lors de l'initialisation du microcontrôleur (ici pour mettre la broche sur laquelle est branchée la diode en mode écriture), puis la fonction \texttt{loop()} est appelée en boucle (ici, elle allume un signal sur la broche associée à la diode, effectue une pause d'une seconde, éteint le signal, et effectue une nouvelle pause d'une seconde).

Si le code présenté ici est assez simple, il peut rapidement se complexifier dès lors que l'utilisateur souhaite contrôler plusieurs montages différents en parallèle, et en particulier à des fréquences différentes : on ne peut alors plus utiliser la fonction \texttt{delay()}, qui bloque l'exécution du reste du programme (on dit qu'elle est \textit{bloquante}). L'origine de ce problème est le fait que le langage C ne supporte pas le parallélisme et qu'il faut donc réaliser l'ordonnancement du code soi-même.

Le projet de recherche encadré de Léo Boitel propose une solution à ce problème en montrant que l'on peut implémenter le \autoref{code:clignotement-diode} en Heptagon. Heptagon~\cite{HeptagonBZR} est un langage synchrone et réactif développé pour la recherche, inspiré de Lustre, et dont la syntaxe permet l'expression de structures de contrôles telles que les automates à états. Sa nature réactive en fait un candidat idéal pour résoudre le problème précédent puisqu'elle permet de manipuler des flots de données en parallèle, l'ordonnancement étant effectué au moment de la compilation.

L'objectif de la librairie Reacduino~\cite{aristoteReacduino} est de compléter cette solution en implémentant autant que possible de fonctions de la librairie Arduino en Heptagon, afin de proposer un environnement de développement pour microcontrôleurs Arduino en Heptagon aussi simple à utiliser que possible.

\section{Développement}
\label{sec:developpement}

\subsection{Environnement de développement et compilation}
\label{sec:developpement>environnement-de-developpement-et-compilation}

Reacduino est une librairie écrite dans le langage C. Une interface est donc fournie sous la forme d'un fichier \texttt{reacduino.epi} afin d'indiquer à Heptagon quels nœuds sont définis dans la librairie. Une version plus lisible, disponible en 
\hyperref[module:Reacduino]{Appendice}, peut-être générée via \texttt{odoc}~\cite{Odoc2021} par la commande \texttt{make doc}.

En théorie, il faut compiler ce fichier vers un fichier \texttt{reacduino.epci} à l'aide de la commande \texttt{heptc reacduino.epi} puis passer le dossier contenant ce fichier ainsi que le code source de la librairie à Heptagon à l'aide de l'option \texttt{-I}. En pratique, tout cela est fait automatiquement lors de l'exécution de la commande \texttt{make ept} : il suffit donc d'inclure la ligne \texttt{open Reacduino} au début d'un fichier Heptagon pour pouvoir utiliser les fonctions disponibles dans la librairie.

Lors de la compilation des fichiers Heptagon, le compilateur ne vérifie pas que les fonctions listées dans l'interface \texttt{reacduino.epi} sont bien implémentées en C : c'est le rôle du compilateur C vers Arduino, en l'occurence Ino~\cite{Ino}, qui peut être directement utilisé via les commandes \texttt{make ino} (pour compiler le code) et \texttt{make flash} (pour transférer le binaire compilé à une carte Arduino). À la place, Heptagon génère du code C dans lequel, pour chaque nœud de Reacduino utilisé dans le code Heptagon, les objets suivants sont utilisés :

\begin{itemize}
\item le type \texttt{Reacduino\_\_<nom-du-noeud>\_mem} de la mémoire interne d'une instance du nœud, en pratique une \texttt{struct} contenant les différentes variables utilisées dans le nœud ;
\item le type \texttt{Reacduino\_\_<nom-du-noeud>\_out} de l'objet retourné par une instance du noed, en pratique une \texttt{struct} contenant les différentes variables retournées par le nœud ;
\item la fonction \texttt{Reacduino\_\_<nom-du-noeud>\_reset} qui initialise la mémoire interne d'une instance du nœud, prenant en argument un pointeur vers un objet de type \texttt{Reacduino\_\_<nom-du-noeud>\_mem} et ne retournant rien ;
\item la fonction \texttt{Reacduino\_\_<nom-du-noeud>\_step} qui effectue un pas d'une instance du nœud, prenant en argument les arguments du nœud ainsi que deux pointeurs vers des objets de type \texttt{Reacduino\_\_<nom-du-noeud>\_mem} et \texttt{Reacduino\_\_<nom-du-noeud>\_out} et ne retournant rien.
\end{itemize}
Pour les fonctions (les nœuds indépendants du temps), Heptagon n'utilise pas le type de la mémoire interne ni la fonction d'initialisation. Pour chaque type que Reacduino définit, Heptagon utilise le type \texttt{Reacduino\_\_<nom-du-type>} qui est une énumération des objets définis par ce type, eux-même utilisés sous la forme \texttt{Reacduino\_\_<nom-de-l'objet>} (cela implique notamment que deux objets de types différents ne peuvent pas avoir le même nom). Enfin, pour les nœuds paramétriques, le langage C ne supportant pas l'instanciation paramétrique de fonctions, Heptagon utilise les mêmes objets que pour les nœuds classiques mais concatène les paramètres au nom du nœud. Par exemple la fonction d'initialisation d'un nœud \texttt{myNode<<0,0>>()} est \texttt{Reacduino\_\_myNode\\\_params\_0\_0\_reset}.

Cependant pour utiliser des nœuds paramétriques Heptagon requiert un fichier binaire \texttt{reacduino.cpo} supplémentaire qui contient vraisemblablement les instanciations des nœuds avec les paramètres utilisés dans le programme. Heptagon ne permettant pas de créer un tel fichier à partir de la seule interface \texttt{reacduino.epi} (un fichier \texttt{.ept} est nécessaire), les nœuds paramétriques définis par Reacduino ne sont en pratique pas utilisables.

\subsection{Implémentation}
\label{sec:developpement>implementation}

En pratique, écrire une librairie pour Heptagon en C consiste donc à implémenter les objets utilisés par Heptagon et listés dans la section précédente. Dans le cas de Reacduino, cette implémentation peut être trouvée dans le dossier \texttt{lib/reacduino}. La plupart des fonctions disponibles dans la librairie Arduino étant non bloquantes, elles peuvent donc être directement appelées dans le corps de la fonction \texttt{Reacduino\_\_<nom-du-noeud>\_step} correspondante, et la librairie ne fournit donc qu'une interface entre Heptagon et la librairie Arduino implémentée en C.

Cependant, en interfaçant les fonctions de la sorte, certains problèmes peuvent survenir si le programmeur n'est pas suffisamment prudent, et il faut aussi traiter le cas des fonctions qui ne peuvent tout simplement pas être interfacées (par exemple parce qu'elles sont bloquantes). L'écriture de Reacduino n'a donc pas non plus été triviale.

\subsubsection{Problèmes de types}
\label{sec:developpement>implementation>problemes-de-types}

Deux types de problèmes liés au typage peuvent survenir lors de l'interfaçage des fonctions de la librairie Arduino.

Le premier de ces problèmes est dû au fait que le seul type disponible pour les entiers en Heptagon est le type \texttt{int} qui correspond au type \texttt{int} de C, c'est-à-dire aux entiers implémentés sur 16 bits. De même le seul type disponible pour les flottants est le type \texttt{float}. Cela peut poser problème car les fonctions de la librairie Arduino, par exemple \texttt{millis} (\ref{val:Reacduino.millis}) qui compte le nombre de millisecondes écoulées depuis le démarrage du programme ou encore les fonctions mathématiques (\ref{val:Reacduino.intOfFloat}) utilisent les types \texttt{unsigned long} et \texttt{double}, contenus sur 32 à 64 bits selon l'architecture de la machine. Il faut donc tronquer les sorties des fonctions de la librairie Arduino avant de les passer en sortie des nœuds Heptagon. En pratique le compilateur C fait cela automatiquement, mais pour plus de clarté cela a aussi été fait manuellement dans la librairie Reacduino. Le fait que les types d'Heptagon soient aussi limités peut aussi entraîner des problèmes de précision ou de débordement d'entiers pour l'utilisateur, comme dans l'\hyperref[sec:exemples>clignotement-defectueux]{exemple~\ref*{sec:exemples>clignotement-defectueux} }. Enfin l'absence de types plus avancés comme les chaînes de caractères empêche l'interfaçage des fonctions de la librairie Arduino qui les manipulent.

Le second de ces problèmes est dû à une insuffisance des types dans la librairie Arduino. Selon la carte utilisée, certaines broches peuvent être disponibles et d'autres non, et les modes dans lesquels elles peuvent être utilisées (communication numérique, lecture analogique, écriture d'ondes pulsées, interruptions) varient aussi d'une carte à l'autre. Dans la librairie Arduino, aucune distinction n'est faite au niveau des types puisque les broches sont représentées par des entiers, et le compilateur ne donne aucun avertissement lors de l'utilisation d'une broche qui n'est pas présente sur la carte utilisée, ou qui ne supporte pas le mode dans lequel on l'utilise, ce qui peut donc donner lieu à du code défectueux. Ce problème est partiellement corrigé dans Reacduino puisqu'un type de broche est défini pour chaque mode (\ref{type:Reacduino.gpio-underscorepin}), ce qui empêche par exemple l'utilisation de la fonction \texttt{analogRead} avec une broche qui ne supporte la lecture analogique. Toutes les broches listées dans ces différents types ne sont pas nécessairement disponibles pour toutes les cartes, mais puisque les objets correspondants en C sont générés par le préprocesseur au moment de la compilation, si l'utilisateur essaie d'utiliser une broche qui n'est pas disponible sur la carte pour laquelle il compile une erreur surviendra au moment de la compilation. Cette génération fonctionne quelle que soit la carte utilisée sauf pour les broches supportant l'interruption qui correspondent toujours à celles disponibles sur la carte Arduino Uno (c'est-à-dire les broches numériques 2 et 3). Cette limitation est due au fait qu'il n'y a pas de macro préexistante pour déterminer si une broche supporte les interruptions sur une carte donnée et qu'implémenter une telle macro n'est pas l'objectif principal du développement de Reacduino.

\subsubsection{Interfaçage de librairies externes à Arduino}
\label{sec:developpement>interfacage-de-librairies-externes-arduino}

En plus de la librairie de base, de nombreuses librairies supplémentaires ont été créées pour faciliter le développement d'applications spécifiques sur les cartes Arduino~\cite{LibrariesArduinoReference}. Une de ces librairies est la librairie \texttt{Serial}, qui permet de communiquer avec un ordinateur via son port USB. Dans le cadre du développement de Reacduino, une interface a été créée pour permettre son utilisation dans du code Heptagon (elle est disponible dans la branche \texttt{reacduino\_serial} du dépôt Git~\cite{aristoteReacduino}). Malheureusement, un appel à cette librairie se fait via un appel à une classe en C++, un langage vers lequel Heptagon ne peut pas compiler : cette interface n'est donc pas utilisable.

\subsubsection{Réimplémentation des fonctions bloquantes}
\label{sec:developpement>reimplementation-des-fonctions-bloquantes}

Certaines fonctions de la librairie Arduino ne peuvent tout simplement pas être interfacées car elles sont bloquantes : elles bloquent l'exécution de la suite du programme pendant un temps non négligeable. C'est par exemple le cas des fonctions \texttt{pulseIn} et \texttt{delay}. Il faut alors en implémenter des versions parallélisables, voire modifier leurs sémantiques pour les rendre plus adaptées à un modèle réactif synchrone.

Un appel à la fonction \texttt{pulseIn(pin, value)} bloque l'exécution du programme jusqu'à ce que le signal \texttt{value} soit detecté sur la broche \texttt{pin}, moment auquel il renvoie la durée de ce signal. Une première façon d'adapter cette fonction est de changer sa sémantique pour qu'elle ne compte plus le temps mais seulement le nombre de pas effectué dans l'horloge Heptagon. C'est ce que fait le nœud \texttt{pulseInTicks} de Reacduino (\ref{val:Reacduino.pulseInTicks}). Une seconde manière de l'adapter est de la rendre parallèlisable. Si la fonction doit être utilisée pour mesurer des temps longs (par rapport à la durée d'exécution d'un pas), elle peut facilement être réimplémentée de manière non bloquante avec les nœuds d'interruptions fournis par Reacduino (détaillés en \autoref{sec:developpement>gestion-des-interruptions}). Pour les temps plus courts, la fonction n'est pas considérée bloquante puisqu'elle ne rallonge pas beaucoup la durée d'un pas : elle est donc accessible via un nœud Reacduino à utiliser avec précaution (\ref{val:Reacduino.pulseIn}).

De même, un appel à la fonction \texttt{delay(millis)} bloque l'exécution du programme pendant \texttt{millis} millisecondes. Pour adapter cette fonction dans le modèle réactif synchrone de Reacduino, il est possible d'implémenter des nœuds qui renvoient une horloge (un signal booléen) qui n'est activée que périodiquement (\ref{val:Reacduino.millis}). Par exemple l'horloge \texttt{delayInTicks(n)} est activée une fois tous les \texttt{n} pas de l'horloge Heptagon, et l'horloge \texttt{delay(millis)} est activée (en moyenne) une fois toutes les \texttt{millis} ms. Pour simplifier l'expérience de l'utilisateur, l'horloge \texttt{delayOnce(millis)}, activée seulement au bout de \texttt{millis} ms, est aussi implémentée, ainsi que des équivalents de ces nœuds mais prenant en argument un nombre de microsecondes.

\subsubsection{Gestion des interruptions}
\label{sec:developpement>gestion-des-interruptions}

La gestion des interruptions peut sembler à première vue incompatible avec le modèle synchrone d'Heptagon puisqu'elle requiert l'exécution de code à des instants arbitraires et rends donc impossible l'ordonnancement du programme. Cependant en pratique le système est un système à temps réel et les interruptions sont donc particulièrement utiles, par exemple pour obtenir un programme aussi précis que possible ou pour simplifier le code. Se passer des interruptions serait donc incompatible avec la philosophie de Reacduino.

Un compromis a donc été trouvé : il est toujours possible pour l'utilisateur d'exécuter des nœuds Heptagon lors d'une interruption, mais cela doit être spécifié en C ; en parallèle, Reacduino donne accès à des nœuds qui renvoient des informations sur les interruptions qui ont eu lieu entre le pas précédent et le pas actuel de l'horloge Heptagon (\ref{type:Reacduino.isr-underscoremode}), comme le nombre d'interruptions qui ont eu lieu ou l'instant auquel la dernière interruption a eu lieu.

Cepdendant, l'implémentation de la fonction d'initialisation \texttt{Reacduino\_\_<nom-du-noeud>\_reset} n'a pas accès aux arguments du nœud et ne peut donc à priori pas attacher d'interruption sur une broche spécifiée en argument. Il faut donc utiliser des nœuds paramétriques. Malheureusement, comme on l'a vu en \autoref{sec:developpement>environnement-de-developpement-et-compilation}, le langage C ne supporte pas la définition paramétrique de fonctions et les paramètres sont passés dans le nom des objets C correspondants. Il faut donc avoir recours à des macros, afin de ne pas avoir à réécrire quasiment le même code pour chaque paramètre disponible d'une part, et afin de ne définir une instance d'un nœud avec une certaine broche seulement lorsque ladite broche supporte les interruptions d'autre part. Cela a le défaut de rendre le code moins lisible.

Si, comme expliqué en \autoref{sec:developpement>environnement-de-developpement-et-compilation}, ces nœuds paramétriques ne peuvent pas être utilisés à cause d'une insuffisance du compilateur Heptagon, il est possible de les utiliser indirectement en définissant des nœuds (non paramétriques) qui interfacent des instances spécifiques de ces nœuds paramétriques, comme c'est par exemple le cas dans la librairie locale de l'\hyperref[sec:exemples>interrupteur]{exemple~\ref*{sec:exemples>interrupteur} }.

\section{Exemples}
\label{sec:exemples}

Le code source de Reacduino est accompagné de plusieurs exemples, disponibles dans le dossier \texttt{examples/}, qui montrent comment utiliser la librairie ainsi que ses fonctionnalités et insuffisances. 

\subsection{Gabarit}
\label{sec:exemples>gabarit}

Pour utiliser du code Heptagon avec Ino, il faut commencer par inclure le code C généré par Heptagon : c'est ce qui est fait dans le \autoref{code:gabarit-src-sketch.h}.

\begin{code}
  \captionof{listing}{Gabarit : \texttt{src/sketch.h}}
  \label{code:gabarit-src-sketch.h}
  \inputexample[arduino]{template/src/sketch.h}
\end{code}

La structure \texttt{extern "C"} est utilisée car Ino interprète le code comme du C++ par défaut.

Il est ensuite possible de faire référence au code C généré par Heptagon : il suffit alors d'instancier la mémoire interne et la sortie du nœud principal (\texttt{main} par défaut) et d'appeler la fonction d'initialisation dans la phase d'initialisation et la fonction de pas dans la phase de boucle, comme dans le \autoref{code:gabarit-src-sketch.ino}.

\begin{code}
  \captionof{listing}{Gabarit : \texttt{src/sketch.ino}}
  \label{code:gabarit-src-sketch.ino}
  \inputexample[arduino]{template/src/sketch.ino}
\end{code}

Dans cet exemple le nœud principal est vide (\autoref{code:gabarit-src-controller-controller.ept}), donc en particulier il n'utilise pas de mémoire interne. Pour cette raison, Heptagon ne génère pas d'objet de mémoire interne, et il faut donc supprimer dans le \autoref{code:gabarit-src-sketch.ino} les lignes 3 et 7 ainsi que l'instance de \texttt{\&mem} à la ligne 11 pour obtenir un code correct.

\begin{code}
  \captionof{listing}{Gabarit : \texttt{src/controller/controller.ept}}
  \label{code:gabarit-src-controller-controller.ept}
  \inputexample{template/src/controller/controller.ept}
\end{code}

\subsection{Clignotement simple}
\label{sec:exemples>clignotement-simple}

Cet exemple montre comment appeler les nœuds Reacduino dans un fichier Heptagon. L'objectif est de réimplémenter le \autoref{code:clignotement-diode} en Heptagon de façon naïve.

Comme le montre le \autoref{code:clignotement-simple-src-controller-controller.ept}, il faut commencer par ouvrir Reacduino, et il est alors possible de faire référence aux fonctions disponibles dans la librairie par leur nom.

\begin{code}
  \captionof{listing}{Clignotement simple : \texttt{src/controller/controller.ept}}
  \label{code:clignotement-simple-src-controller-controller.ept}
  \inputexample{blink_simple/src/controller/controller.ept}
\end{code}

Comme expliqué en \autoref{sec:developpement>implementation>problemes-de-types}, les broches sont réparties par fonctionnalités dans plusieurs types différents (\ref{type:Reacduino.gpio-underscorepin}), et il faut ainsi spécifier le mode dans lequel on veut utiliser une broche pour lui faire référence.

Cet exemple n'est pas une solution au problème de parallélisme introduit en \autoref{sec:introduction} puisque la fonction \texttt{delay} est toujours utilisée pour choisir la période de clignotement dans le \autoref{code:clignotement-simple-src-sketch.ino}.

\begin{code}
  \captionof{listing}{Clignotement simple : \texttt{src/sketch.ino}}
  \label{code:clignotement-simple-src-sketch.ino}
  \inputexample[arduino]{blink_simple/src/sketch.ino}
\end{code}


\subsection{Clignotement défectueux}
\label{sec:exemples>clignotement-defectueux}

Pour résoudre le problème de parallèlisme introduit en \autoref{sec:introduction}, il est possible de gérer la question du temps en Heptagon à l'aide du nœud \texttt{millis} de Reacduino (\ref{val:Reacduino.millis}). C'est ce qui est fait dans le \autoref{code:clignotement-defectueux-src-controller-controller.ept}.

\begin{code}
  \captionof{listing}{Clignotement défectueux : \texttt{src/controller/controller.ept}}
  \label{code:clignotement-defectueux-src-controller-controller.ept}
  \inputexample{blink_faulty/src/controller/controller.ept}
\end{code}

Si ce code est correct, il faut être prudent. L'interface sérielle (accessible par exemple avec la commande \texttt{ino serial}) permet en effet de constater que les valeurs de \texttt{millis} et \texttt{time\_next} débordent au bout de 32 secondes. Ainsi, si, à la ligne 9, l'expression \texttt{millis() - pre(time\_next) >= 0} avait été remplacée par la plus intuitive \texttt{millis() >= pre(time\_next)}, le code n'aurait pas été correct : \texttt{time\_next} déborde en effet le premier et tant que \texttt{millis()} n'a pas débordé l'expression \texttt{millis() >= pre(time\_next)} est toujours vraie, et ainsi à chaque pas pendant 500 millisecondes la diode clignote et \texttt{time\_next} est incrémenté. Cela a finalement pour effet d'arrêter le clignotement de la diode pendant environ 5 secondes, le temps pour \texttt{millis()} de rejoindre \texttt{time\_next} qui a pris de l'avance en étant incrémenté.

Comme expliqué en \autoref{sec:developpement>implementation>problemes-de-types}, ce problème (il n'est pas souhaitable que l'utilisateur ait besoin de faire attention à ce genre de détails) est dû à une insuffisance de Heptagon dans les types proposés.

\subsection{Clignotement parallèle}
\label{sec:exemples>clignotement-parallele}


L'\hyperref[sec:exemples>clignotement-defectueux]{exemple~\ref*{sec:exemples>clignotement-defectueux} } n'a pas besoin d'être aussi détaillé : à la place de faire des calculs à l'aide de \texttt{millis}, il est tout à fait possible d'utiliser le nœud \texttt{delay} qui fait ces calculs pour l'utilisateur. Cette façon de faire, utilisée dans le \autoref{code:clignotement-parallele-src-controller-controller.ept}, en plus d'être plus simple, permet aussi de limiter (partiellement) les problèmes dûs aux débordement de \texttt{millis}.

\begin{code}
  \captionof{listing}{Clignotement parallèle : \texttt{src/controller/controller.ept}}
  \label{code:clignotement-parallele-src-controller-controller.ept}
  \inputexample{blink_parallel/src/controller/controller.ept}
\end{code}

La véritable puissance de cet exemple, c'est qu'il est très facile de fusionner plusieurs montages en parallèle : il suffit de rajouter un nœud et une ligne dans le nœud \texttt{main} pour chaque montage, et Heptagon se charge d'ordonnancer le tout. Ce serait bien plus fastidieux à faire directement en C.

\subsection{Sirène}
\label{sec:exemples>sirene}

L'objectif de cet exemple est de faire varier la luminosité d'une diode périodiquement. Il permet de montrer qu'il faut rester prudent quand à l'utilisation des fonctions de la librairie Arduino dont le comportement n'est pas toujours fidèle à l'intuition.

Pour faire varier une valeur périodiquement, le premier réflexe est d'utiliser une fonction trigonométrique, comme dans le \autoref{code:sirene-defectueuse-src-controller-controller.ept}.

\begin{code}
  \captionof{listing}{Sirène défectueuse : \texttt{src/controller/controller.ept}}
  \label{code:sirene-defectueuse-src-controller-controller.ept}
  \inputexample{fade_faulty/src/controller/controller.ept}
\end{code}

Cependant ce programme n'a pas l'effet escompté : la luminosité de la diode varie bien périodiquement, mais cette variation n'est pas du tout continue. L'origine de ce problème est le fait que les microcontrôleurs des cartes Arduino ne sont pas très puissants, et la calcul de la fonction \texttt{sin} prends donc plusieurs cycles, pendant lesquels la luminosité de la diode n'est pas mise à jour.

Pour corriger ce problème, il faut donc par exemple avoir recours à un signal triangulaire créé manuellement, comme dans le \autoref{code:sirenesrc-controller-controller.ept}.

\begin{code}
  \captionof{listing}{Sirène : \texttt{src/controller/controller.ept}}
  \label{code:sirenesrc-controller-controller.ept}
  \inputexample{fade/src/controller/controller.ept}
\end{code}

\subsection{Multiplexeur}
\label{sec:exemples>multiplexeur}

Cet exemple montre une particularité des langages réactifs qu'il est important de rappeler à un nouvel utilisateur : le conditionnement (\texttt{if ... then ... else ...}) se comporte comme un multiplexeur. On pourrait en effet attendre du \autoref{code:multiplexeur-src-controller-controller.ept} que la diode reste éteinte.

\begin{code}
  \captionof{listing}{Multiplexeur : \texttt{src/controller/controller.ept}}
  \label{code:multiplexeur-src-controller-controller.ept}
  \inputexample{unit_if-then-else_bug/src/controller/controller.ept}
\end{code}

Au contraire, la diode reste allumée. La raison pour cela est que Heptagon compile le conditionnement comme un multiplexeur : les deux branches sont exécutées, et la valeur correspondant à la condition est retournée. L'ordonnancement fait donc ici que \texttt{digitalWrite(GPIO\_D13,false)} est exécuté avant \texttt{digitalWrite(GPIO\_D13,true)}, et la diode reste allumée.

\subsection{Interrupteur}
\label{sec:exemples>interrupteur}

L'exemple dont est tiré le \autoref{code:interrupteur-src-controller-controller.ept} démontre qu'il est possible d'utiliser des interruptions en Heptagon pur pour effectuer certaines tâches, ici allumer et éteindre une diode lorsque l'on appuie sur un bouton.

\begin{code}
  \captionof{listing}{Interrupteur : \texttt{src/controller/controller.ept}}
  \label{code:interrupteur-src-controller-controller.ept}
  \inputexample{button/src/controller/controller.ept}
\end{code}

Puisque Heptagon ne permet pas de compiler des librairies qui définissent des nœuds paramétriques, une librairie locale est rajoutée pour instancier le code C des nœuds paramétriques requis avec des paramètres spécifiques, et ainsi de rendre ces nœuds accessibles via des nœuds non paramétriques. Elle définit donc la fonction de pas \texttt{Local\_\_onInterrupt\_D2\_RISING\_step} qui appelle la fonction de pas du nœud \texttt{onInterrupt} de Reacduino instanciée avec les paramètres \texttt{ISR\_D2} et \texttt{RISING}.

\subsection{Ultrasons}
\label{sec:exemples>ultrasons}

Les nœuds gérant les interruptions en Heptagon ne sont malheureusement pas adaptés à toutes les tâches. Par exemple, pour utiliser un capteur ultrasonique pour mesurer une distance, toutes les interruptions à attraper se font entre les deux mêmes pas si la distance est suffisamment courte, et le Programme~\ref{code:ultrasons-src-controller-controller.ept} n'est donc pas correct, puisque l'exécution reste bloquée à l'état \texttt{WaitEndAnswer}.

\begin{code}
  \captionof{listing}{Ultrasons : \texttt{src/controller/controller.ept}}
  \label{code:ultrasons-src-controller-controller.ept}
  \inputexample{ultrasound/src/controller/controller.ept}
\end{code}

Il serait évidemment possible de compresser l'automate (quitte à rendre le code moins accessible), mais il serait toujours impossible d'enregistrer les deux valeurs nécessaires car le nœud \texttt{lastInterrupt} ne peut donner accès qu'à une seule de ces deux valeurs.

L'utilisateur peut cependant toujours utiliser la fonction \texttt{pulseIn} (qui serait traditionnellement utilisée en C pour effectuer cette tâche) puisque dans ce cas précis elle ne devrait pas beaucoup ralentir l'exécution d'un pas.

\section{Conclusion}
\label{sec:conclusion}

Le problème de parallélisme introduit en \autoref{sec:introduction} peut donc être quasiment résolu à l'aide de la librairie Reacduino et du langage Heptagon : la majorité des fonctions C de la librairie Arduino peuvent être rendues accessibles via une interface relativement simple, et les fonctions bloquantes peuvent être parallélisées ; la gestion des interruptions pose plus de problèmes car leur modèle à temps réel n'est pas entièrement compatible avec le modèle synchrone de Heptagon, mais un compromis peut être trouvé.

Les difficultés d'utilisation de la librairie Reacduino sont ainsi principalement dues aux insuffisances de Heptagon : l'étendre pour permettre la compilation vers du C++ et pour compléter les types de bases permettrait de résoudre une grande majorité des difficultés qui ne sont pas déjà présentent lorsque l'utilisateur programme en C.



\printbibliography

\appendix 
\subfile{../odoc/reacduino/doc} 

\end{document}