This example shows how the lack of different types in the Heptagon language (currently, only `int` is supported for integer values) makes it harder to have correct programs.

Indeed, slightly changing line 8 in `controller.ept`, replacing
```
trigger_next = false -> millis() - pre(time_next) >= 0
```
by
```
trigger_next = false -> millis() >= pre(time_next)
```
creates a bug after 33s : `time_next` overflows and becomes negative while, for at least 500ms, since `millis()` has not overflowed, hence `millis() >= pre(time_next)` is true at each step and `time_next` is thus incremented at each step. In practice this leads the LED to stop blinking for ~5s. Using `micros()`, the program would fail after 5min. 

Conversely, `unsigned long` integers on a 64-bit machine, the second code would (in practice) never fail.
