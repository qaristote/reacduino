An advanced version of `blink_simple` that is non blocking : other nodes can be executed in parallel without having to wait 500ms between each step.

The code is basically the same as in `blink_faulty`, except that the computation of the delay is done in a C function to avoid integer overflows.
