#include "sketch.h"

Controller__main_mem mem ;
Controller__main_out out ;

void setup() {
	Controller__main_reset(&mem) ;
}

void loop() {
	Controller__main_step(&out, &mem) ;
}
