#ifndef LOCAL_TYPES_H
#define LOCAL_TYPES_H

#include "../reacduino/interrupts/reacduino_interrupts_types.h"

typedef Reacduino__onInterrupt_params_ISR_D2RISING_mem Local__onInterrupt_D2_RISING_mem ;
typedef Reacduino__onInterrupt_params_ISR_D2RISING_out Local__onInterrupt_D2_RISING_out ;

#endif
