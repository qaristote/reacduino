#include "local.h"
#include "../reacduino/interrupts/reacduino_interrupts.h"

void Local__onInterrupt_D2_RISING_reset(Local__onInterrupt_D2_RISING_mem *mem) {
  Reacduino__onInterrupt_params_ISR_D2RISING_reset((Reacduino__onInterrupt_params_ISR_D2RISING_mem*) mem) ;
}
void Local__onInterrupt_D2_RISING_step(Local__onInterrupt_D2_RISING_out *out,
				       Local__onInterrupt_D2_RISING_mem *mem) {
  Reacduino__onInterrupt_params_ISR_D2RISING_step((Reacduino__onInterrupt_params_ISR_D2RISING_out*) out,
						  (Reacduino__onInterrupt_params_ISR_D2RISING_mem*) mem) ;
}

void Local__lastInterrupt_D2_RISING_reset(Local__lastInterrupt_D2_RISING_mem *mem) {
  Reacduino__lastInterrupt_params_ISR_D2RISING_reset((Reacduino__lastInterrupt_params_ISR_D2RISING_mem*) mem) ;
}
void Local__lastInterrupt_D2_RISING_step(Local__lastInterrupt_D2_RISING_out *out,
					 Local__lastInterrupt_D2_RISING_mem *mem) {
  Reacduino__lastInterrupt_params_ISR_D2RISING_step((Reacduino__lastInterrupt_params_ISR_D2RISING_out*) out,
						    (Reacduino__lastInterrupt_params_ISR_D2RISING_mem*) mem) ;
}
