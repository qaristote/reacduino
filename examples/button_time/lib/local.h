#ifndef LOCAL_H
#define LOCAL_H

#include "local_types.h"

void Local__onInterrupt_D2_RISING_reset(Local__onInterrupt_D2_RISING_mem *mem) ;
void Local__onInterrupt_D2_RISING_step(Local__onInterrupt_D2_RISING_out *out,
				       Local__onInterrupt_D2_RISING_mem *mem) ;

void Local__lastInterrupt_D2_RISING_reset(Local__lastInterrupt_D2_RISING_mem *mem) ;
void Local__lastInterrupt_D2_RISING_step(Local__lastInterrupt_D2_RISING_out *out,
					 Local__lastInterrupt_D2_RISING_mem *mem) ;

#endif
