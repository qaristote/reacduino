#include "local.h"
#include "../reacduino/interrupts/reacduino_interrupts.h"

void Local__countInterrupts_D2_CHANGE_reset(Local__countInterrupts_D2_CHANGE_mem *mem) {
  Reacduino__countInterrupts_params_ISR_D2CHANGE_reset((Reacduino__countInterrupts_params_ISR_D2CHANGE_mem*) mem) ;
}
void Local__countInterrupts_D2_CHANGE_step(Local__countInterrupts_D2_CHANGE_out *out,
				       Local__countInterrupts_D2_CHANGE_mem *mem) {
  Reacduino__countInterrupts_params_ISR_D2CHANGE_step((Reacduino__countInterrupts_params_ISR_D2CHANGE_out*) out,
						  (Reacduino__countInterrupts_params_ISR_D2CHANGE_mem*) mem) ;
}

void Local__onInterrupt_D2_CHANGE_reset(Local__onInterrupt_D2_CHANGE_mem *mem) {
  Reacduino__onInterrupt_params_ISR_D2CHANGE_reset((Reacduino__onInterrupt_params_ISR_D2CHANGE_mem*) mem) ;
}
void Local__onInterrupt_D2_CHANGE_step(Local__onInterrupt_D2_CHANGE_out *out,
				       Local__onInterrupt_D2_CHANGE_mem *mem) {
  Reacduino__onInterrupt_params_ISR_D2CHANGE_step((Reacduino__onInterrupt_params_ISR_D2CHANGE_out*) out,
						  (Reacduino__onInterrupt_params_ISR_D2CHANGE_mem*) mem) ;
}

void Local__lastInterrupt_D2_CHANGE_reset(Local__lastInterrupt_D2_CHANGE_mem *mem) {
  Reacduino__lastInterrupt_params_ISR_D2CHANGE_reset((Reacduino__lastInterrupt_params_ISR_D2CHANGE_mem*) mem) ;
}
void Local__lastInterrupt_D2_CHANGE_step(Local__lastInterrupt_D2_CHANGE_out *out,
					 Local__lastInterrupt_D2_CHANGE_mem *mem) {
  Reacduino__lastInterrupt_params_ISR_D2CHANGE_step((Reacduino__lastInterrupt_params_ISR_D2CHANGE_out*) out,
						    (Reacduino__lastInterrupt_params_ISR_D2CHANGE_mem*) mem) ;
}
