#ifndef LOCAL_H
#define LOCAL_H

#include "local_types.h"

void Local__countInterrupts_D2_CHANGE_reset(Local__countInterrupts_D2_CHANGE_mem *mem) ;
void Local__countInterrupts_D2_CHANGE_step(Local__countInterrupts_D2_CHANGE_out *out,
				       Local__countInterrupts_D2_CHANGE_mem *mem) ;


void Local__onInterrupt_D2_CHANGE_reset(Local__onInterrupt_D2_CHANGE_mem *mem) ;
void Local__onInterrupt_D2_CHANGE_step(Local__onInterrupt_D2_CHANGE_out *out,
				       Local__onInterrupt_D2_CHANGE_mem *mem) ;

void Local__lastInterrupt_D2_CHANGE_reset(Local__lastInterrupt_D2_CHANGE_mem *mem) ;
void Local__lastInterrupt_D2_CHANGE_step(Local__lastInterrupt_D2_CHANGE_out *out,
					 Local__lastInterrupt_D2_CHANGE_mem *mem) ;

#endif
