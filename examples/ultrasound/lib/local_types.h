#ifndef LOCAL_TYPES_H
#define LOCAL_TYPES_H

#include "../reacduino/interrupts/reacduino_interrupts_types.h"

typedef Reacduino__countInterrupts_params_ISR_D2CHANGE_mem Local__countInterrupts_D2_CHANGE_mem ;
typedef Reacduino__countInterrupts_params_ISR_D2CHANGE_out Local__countInterrupts_D2_CHANGE_out ;

typedef Reacduino__onInterrupt_params_ISR_D2CHANGE_mem Local__onInterrupt_D2_CHANGE_mem ;
typedef Reacduino__onInterrupt_params_ISR_D2CHANGE_out Local__onInterrupt_D2_CHANGE_out ;

typedef Reacduino__lastInterrupt_params_ISR_D2CHANGE_mem Local__lastInterrupt_D2_CHANGE_mem ;
typedef Reacduino__lastInterrupt_params_ISR_D2CHANGE_out Local__lastInterrupt_D2_CHANGE_out ;

#endif
