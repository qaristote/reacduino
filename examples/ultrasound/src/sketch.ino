#include "sketch.h"

Controller__main_mem mem ;
Controller__main_out out ;

void setup() {
  Serial.begin(9600) ;
  Controller__main_reset(&mem) ;
}

void loop() {
  Controller__main_step(&out, &mem) ;
  Serial.print(out.curr_state) ;
  Serial.print(" : ") ;
  Serial.print(out.start) ;
  Serial.print(" -> ") ;
  Serial.print(out.stop) ;
  Serial.print(" : + ") ;
  Serial.print(out.signal) ;
  Serial.print(" = ") ;
  Serial.print(out.interrupts) ;
  Serial.println() ;
}
