This examples shows a property of Heptagon that may not be obvious to users coming from more classic languages. 

Here `if ... then ... else ...` acts as a multiplexer, meaning it first computes both branches (in case some of them are history dependant) and then computes the conditional expression. 
