#include <stdbool.h>
#include "Arduino.h"
#include "reacduino_advanced_io.h"
#include "../pins/reacduino_pins.h"

void Reacduino__noTone_step(Reacduino__gpio_pin pin,
			    Reacduino__noTone_out *out) {
  noTone(getGPIOPinNumber(pin)) ;
}

void Reacduino__tone_step(Reacduino__gpio_pin pin,
			  int frequency,
			  int duration,
			  Reacduino__tone_out *out) {
  if (duration == 0) {
    noTone(getGPIOPinNumber(pin), (unsigned int) frequency) ;
  } else {
    noTone(getGPIOPinNumber(pin), (unsigned int) frequency, (unsigned long) duration) ;
  } ;
}

void Reacduino__pulseInTicks_reset(Reacduino__pulseInTicks_mem *self) {
  self->ticks = 0 ; 
}
void Reacduino__pulseInTicks_step(bool signal,
				  Reacduino__pulseInTicks_out *out,
				  Reacduino__pulseInTicks_mem *self) {
  if (signal) {
    self->ticks ++ ;
  } else {
    self->ticks = 0 ;
  } ;
  out->value = self->ticks ;
}

void Reacduino__pulseIn_step(Reacduino__gpio_pin pin,
			     bool value,
			     int timeout,
			     Reacduino__pulseIn_out* out) {
  out->value = (int) pulseIn(getGPIOPinNumber(pin), value, (unsigned long) timeout) ;
}

