#ifndef REACDUINO_ADVANCED_IO_H
#define REACDUINO_ADVANCED_IO_H

#include <stdbool.h>
#include "reacduino_advanced_io_types.h"

void Reacduino__noTone_step(Reacduino__gpio_pin pin,
			    Reacduino__noTone_out *out) ;

void Reacduino__tone_step(Reacduino__gpio_pin pin,
			  int frequency,
			  int duration,
			  Reacduino__tone_out *out) ;

void Reacduino__pulseInTicks_reset(Reacduino__pulseInTicks_mem *self) ;
void Reacduino__pulseInTicks_step(bool signal,
				  Reacduino__pulseInTicks_out *out,
				  Reacduino__pulseInTicks_mem *self) ;

void Reacduino__pulseIn_step(Reacduino__gpio_pin pin,
			     bool value,
			     int timeout,
			     Reacduino__pulseIn_out* out) ;

#endif
