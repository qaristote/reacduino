#ifndef REACDUINO_ADVANCED_IO_TYPES_H
#define REACDUINO_ADVANCED_IO_TYPES_H

#include "../reacduino_types.h"

typedef unit_out Reacduino__noTone_out ;

typedef unit_out Reacduino__tone_out ;

typedef struct Reacduino__pulseInTicks_mem {
  int ticks ;
} Reacduino__pulseInTicks_mem ;
typedef int_out Reacduino__pulseInTicks_out ;

typedef int_out Reacduino__pulseIn_out ;
/*
#define DEFINE_PULSEINMILLIS_TYPES(p) \
  typedef struct Reacduino__pulseInMillis_params_GPIO_ ## p ## _mem {	\
    volatile int last_fall ;					\
    volatile int last_rise ;					\
  } Reacduino__pulseInMillis_params_GPIO_ ## p ## _mem ;		\
  typedef struct Reacduino__pulseInMillis_params_GPIO_ ## p ## _out { \
    int duration ;						\
  } Reacduino__pulseInMillis_params_GPIO_ ## p ## _out ;

#define DEFINE_TYPES_FOR_DIGITAL_PIN(p) DEFINE_PULSEINMILLIS_TYPES(p)

#define DEFINE_TYPES_FOR_ANALOG_INPUT(p) DEFINE_PULSEINMILLIS_TYPES(p)

#if (14 > 0)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D0)
#endif
#if (14 > 1)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D1)
#endif
#if (14 > 2)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D2)
#endif
#if (14 > 3)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D3)
#endif
#if (14 > 4)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D4)
#endif
#if (14 > 5)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D5)
#endif
#if (14 > 6)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D6)
#endif
#if (14 > 7)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D7)
#endif
#if (14 > 8)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D8)
#endif
#if (14 > 9)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D9)
#endif
#if (14 > 10)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D10)
#endif
#if (14 > 11)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D11)
#endif
#if (14 > 12)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D12)
#endif
#if (14 > 13)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D13)
#endif
#if (14 > 14)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D14)
#endif
#if (14 > 15)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D15)
#endif
#if (14 > 16)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D16)
#endif
#if (14 > 17)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D17)
#endif
#if (14 > 18)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D18)
#endif
#if (14 > 19)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D19)
#endif
#if (14 > 20)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D20)
#endif
#if (14 > 21)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D21)
#endif
#if (14 > 22)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D22)
#endif
#if (14 > 23)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D23)
#endif
#if (14 > 24)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D24)
#endif
#if (14 > 25)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D25)
#endif
#if (14 > 26)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D26)
#endif
#if (14 > 27)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D27)
#endif
#if (14 > 28)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D28)
#endif
#if (14 > 29)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D29)
#endif
#if (14 > 30)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D30)
#endif
#if (14 > 31)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D31)
#endif
#if (14 > 32)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D32)
#endif
#if (14 > 33)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D33)
#endif
#if (14 > 34)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D34)
#endif
#if (14 > 35)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D35)
#endif
#if (14 > 36)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D36)
#endif
#if (14 > 37)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D37)
#endif
#if (14 > 38)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D38)
#endif
#if (14 > 39)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D39)
#endif
#if (14 > 40)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D40)
#endif
#if (14 > 41)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D41)
#endif
#if (14 > 42)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D42)
#endif
#if (14 > 43)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D43)
#endif
#if (14 > 44)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D44)
#endif
#if (14 > 45)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D45)
#endif
#if (14 > 46)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D46)
#endif
#if (14 > 47)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D47)
#endif
#if (14 > 48)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D48)
#endif
#if (14 > 49)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D49)
#endif
#if (14 > 50)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D50)
#endif
#if (14 > 51)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D51)
#endif
#if (14 > 52)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D52)
#endif
#if (14 > 53)
  DEFINE_TYPES_FOR_DIGITAL_PIN(D53)
#endif
#if (6 > 0)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A0)
#endif
#if (6 > 1)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A1)
#endif
#if (6 > 2)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A2)
#endif
#if (6 > 3)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A3)
#endif
#if (6 > 4)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A4)
#endif
#if (6 > 5)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A5)
#endif
#if (6 > 6)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A6)
#endif
#if (6 > 7)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A7)
#endif
#if (6 > 8)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A8)
#endif
#if (6 > 9)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A9)
#endif
#if (6 > 10)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A10)
#endif
#if (6 > 11)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A11)
#endif
#if (6 > 12)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A12)
#endif
#if (6 > 13)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A13)
#endif
#if (6 > 14)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A14)
#endif
#if (6 > 15)
  DEFINE_TYPES_FOR_ANALOG_INPUT(A15)
#endif
*/

#endif
