#include "Arduino.h"
#include "reacduino_analog_io.h"
#include "../pins/reacduino_pins.h"

void Reacduino__analogRead_step(Reacduino__analog_pin pin,
				Reacduino__analogRead_out *out) {
  out->value = analogRead(getAnalogPinNumber(pin)) ;
}
  
void Reacduino__analogReference_step(Reacduino__voltage_reference ref,
				     Reacduino__analogReference_out *out) {
  analogReference(_getVoltageReferenceNumber(ref)) ;
}

void Reacduino__analogWrite_step(Reacduino__pwm_pin pin, int value,
				 Reacduino__analogWrite_out *out) {
  analogWrite(getPWMPinNumber(pin), value) ;
}

void Reacduino__analogReadResolution_step(int bits,
					  Reacduino__analogReadResolution_out *out) {
  analogReadResolution(bits) ;
}

void Reacduino__analogWriteResolution_step(int bits,
					   Reacduino__analogWriteResolution_out *out) {
  analogWriteResolution(bits) ;
}

    
int _getVoltageReferenceNumber(Reacduino__voltage_reference ref) {
  switch (ref) {
#ifdef DEFAULT
  case Reacduino__DEFAULT :
    return DEFAULT ;
#endif
#ifdef INTERNAL
  case Reacduino__INTERNAL :
    return INTERNAL ;
#endif
#ifdef INTERNAL0V55
  case Reacduino__INTERNAL0V55 :
    return INTERNAL0V55 ;
#endif
#ifdef INTERNAL1V1
  case Reacduino__INTERNAL1V1 :
    return INTERNAL1V1 ;
#endif
#ifdef INTERNAL1V5
  case Reacduino__INTERNAL1V5 :
    return INTERNAL1V5 ;
#endif
#ifdef INTERNAL2V5
  case Reacduino__INTERNAL2V5 :
    return INTERNAL2V5 ;
#endif
#ifdef INTERNAL2V56
  case Reacduino__INTERNAL2V56 :
    return INTERNAL2V56 ;
#endif
#ifdef INTERNAL4V3
  case Reacduino__INTERNAL4V3 :
    return INTERNAL4V3 ;
#endif
#ifdef EXTERNAL
  case Reacduino__EXTERNAL :
    return EXTERNAL ;
#endif
#ifdef AR_DEFAULT
  case Reacduino__AR_DEFAULT :
    return AR_DEFAULT ;
#endif
#ifdef AR_INTERNAL
  case Reacduino__AR_INTERNAL :
    return AR_INTERNAL ;
#endif
#ifdef AR_INTERNAL1V0
  case Reacduino__AR_INTERNAL1V0 :
    return AR_INTERNAL1V0 ;
#endif
#ifdef AR_INTERNAL1V2
  case Reacduino__AR_INTERNAL1V2 :
    return AR_INTERNAL1V2 ;
#endif
#ifdef AR_INTERNAL1V65
  case Reacduino__AR_INTERNAL1V65 :
    return AR_INTERNAL1V65 ;
#endif
#ifdef AR_INTERNAL2V23
  case Reacduino__AR_INTERNAL2V23 :
    return AR_INTERNAL2V23 ;
#endif
#ifdef AR_INTERNAL2V4
  case Reacduino__AR_INTERNAL2V4 :
    return AR_INTERNAL2V4 ;
#endif
#ifdef AR_EXTERNAL
  case Reacduino__AR_EXTERNAL :
    return AR_EXTERNAL ;
#endif
#ifdef VDD
  case Reacduino__VDD :
    return VDD ;
#endif
  } ;
}
