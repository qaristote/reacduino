#ifndef REACDUINO_ANALOG_IO_H
#define REACDUINO_ANALOG_IO_H

#include "reacduino_analog_io_types.h"

void Reacduino__analogRead_step(Reacduino__analog_pin pin,
				Reacduino__analogRead_out *out) ;

void Reacduino__analogReference_step(Reacduino__voltage_reference ref,
				     Reacduino__analogReference_out *out) ;

void Reacduino__analogWrite_step(Reacduino__pwm_pin pin, int value,
				 Reacduino__analogWrite_out *out) ;

void Reacduino__analogReadResolution_step(int bits,
					  Reacduino__analogReadResolution_out *out) ;

void Reacduino__analogWriteResolution_step(int bits,
					  Reacduino__analogWriteResolution_out *out) ;

#endif
