#ifndef REACDUINO_ANALOG_IO_TYPES_H
#define REACDUINO_ANALOG_IO_TYPES_H

#include "Arduino.h"
#include "../reacduino_types.h"

typedef struct Reacduino__analogRead_out {
  int value ;
} Reacduino__analogRead_out ;

typedef unit_out Reacduino__analogReference_out ;

typedef unit_out Reacduino__analogWrite_out ;

typedef unit_out Reacduino__analogReadResolution_out ;

typedef unit_out Reacduino__analogWriteResolution_out ;

typedef enum {
#ifdef DEFAULT
	      Reacduino__DEFAULT,
#endif
#ifdef INTERNAL
	      Reacduino__INTERNAL,
#endif
#ifdef INTERNAL0V55
	      Reacduino__INTERNAL0V55,
#endif
#ifdef INTERNAL1V1
	      Reacduino__INTERNAL1V1,
#endif
#ifdef INTERNAL1V5
	      Reacduino__INTERNAL1V5,
#endif
#ifdef INTERNAL2V5
	      Reacduino__INTERNAL2V5,
#endif
#ifdef INTERNAL2V56
	      Reacduino__INTERNAL2V56,
#endif
#ifdef INTERNAL4V3
	      Reacduino__INTERNAL4V3,
#endif
#ifdef EXTERNAL
	      Reacduino__EXTERNAL,
#endif
#ifdef AR_DEFAULT
	      Reacduino__AR_DEFAULT,
#endif
#ifdef AR_INTERNAL
	      Reacduino__AR_INTERNAL,
#endif
#ifdef AR_INTERNAL1V0
	      Reacduino__AR_INTERNAL1V0,
#endif
#ifdef AR_INTERNAL1V2
	      Reacduino__AR_INTERNAL1V2,
#endif
#ifdef AR_INTERNAL1V65
	      Reacduino__AR_INTERNAL1V65,
#endif
#ifdef AR_INTERNAL2V23
	      Reacduino__AR_INTERNAL2V23,
#endif
#ifdef AR_INTERNAL2V4
	      Reacduino__AR_INTERNAL2V4,
#endif
#ifdef AR_EXTERNAL
	      Reacduino__AR_EXTERNAL,
#endif
#ifdef VDD
	      Reacduino__VDD,
#endif
} Reacduino__voltage_reference ;

#endif
