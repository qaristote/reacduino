#include <stdbool.h>
#include "reacduino_bytes.h"

void Reacduino__bit_step(int n,
			 Reacduino__bit_out *out) {
  out->value = 1 << n ;
}

void Reacduino__bitClear_step(int x, int n,
			      Reacduino__bitClear_out *out) {
  out->value = x & ~(1 << n) ;
}

void Reacduino__bitRead_step(int x, int n,
			     Reacduino__bitRead_out *out) {
  out->value = (bool) (x & (1 << n)) ;
}

void Reacduino__bitSet_step(int x, int n,
			    Reacduino__bitSet_out *out) {
  out->value = x | (1 << n) ;
}

void Reacduino__bitWrite_step(int x, int n, bool b,
			      Reacduino__bitWrite_out *out) {
  if (b) {
    out->value = x | (1 << n) ;
  } else {
    out->value = x | ~(1 << n) ;
  } ;
}

void Reacduino__bitwiseAnd_step(int x, int y,
				Reacduino__bitwiseAnd_out *out) {
  out->value = x & y ;
}

void Reacduino__bitwiseOr_step(int x, int y,
			       Reacduino__bitwiseOr_out *out) {
  out->value = x | y ;
}

void Reacduino__bitwiseXor_step(int x, int y,
				Reacduino__bitwiseXor_out *out) {
  out->value = x ^ y ;
}

void Reacduino__bitwiseNot_step(int x, int y,
				Reacduino__bitwiseNot_out *out) {
  out->value = ~x ;
}

void Reacduino__bitshiftLeft(int x, int n,
			     Reacduino__bitshiftLeft_out *out) {
  out->value = x << n ;
}

void Reacduino__bitshiftRight(int x, int n,
			      Reacduino__bitshiftRight_out *out) {
  out->value = x >> n ;
}
