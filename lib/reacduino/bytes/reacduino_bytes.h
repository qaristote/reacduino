#ifndef REACDUINO_BYTES_H
#define REACDUINO_BYTES_H

#include <stdbool.h>
#include "reacduino_bytes_types.h"

void Reacduino__bit_step(int n,
			 Reacduino__bit_out *out) ;

void Reacduino__bitClear_step(int x, int n,
			      Reacduino__bitClear_out *out) ;

void Reacduino__bitRead_step(int x, int n,
			     Reacduino__bitRead_out *out) ;

void Reacduino__bitSet_step(int x, int n,
			    Reacduino__bitSet_out *out) ;

void Reacduino__bitWrite_step(int x, int n, bool b,
			      Reacduino__bitWrite_out *out) ;

void Reacduino__bitwiseAnd_step(int x, int y,
				Reacduino__bitwiseAnd_out *out) ;

void Reacduino__bitwiseOr_step(int x, int y,
			       Reacduino__bitwiseOr_out *out) ;

void Reacduino__bitwiseXor_step(int x, int y,
				Reacduino__bitwiseXor_out *out) ;

void Reacduino__bitwiseNot_step(int x, int y,
				Reacduino__bitwiseNot_out *out) ;

void Reacduino__bitshiftLeft(int x, int n,
			     Reacduino__bitshiftLeft_out *out) ;

void Reacduino__bitshiftRight(int x, int n,
			      Reacduino__bitshiftRight_out *out) ;

#endif
