#ifndef REACDUINO_BYTES_TYPES_H
#define REACDUINO_BYTES_TYPES_H

#include "../reacduino_types.h"

typedef byte_out Reacduino__bit_out ;

typedef byte_out Reacduino__bitClear_out ;

typedef byte_out Reacduino__bitRead_out ;

typedef byte_out Reacduino__bitSet_out ;

typedef byte_out Reacduino__bitWrite_out ;

typedef byte_out Reacduino__bitwiseAnd_out ;

typedef byte_out Reacduino__bitwiseOr_out ;

typedef byte_out Reacduino__bitwiseXor_out ;

typedef byte_out Reacduino__bitwiseNot_out ;

typedef byte_out Reacduino__bitshiftLeft_out ;

typedef byte_out Reacduino__bitshiftRight_out ;

#endif
