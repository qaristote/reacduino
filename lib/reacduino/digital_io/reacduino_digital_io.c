#include <stdbool.h>
#include <Arduino.h>
#include "reacduino_digital_io.h"
#include "../pins/reacduino_pins.h"

void Reacduino__digitalRead_step(Reacduino__gpio_pin pin,
				 Reacduino__digitalRead_out *out) {
  int pin_number = getGPIOPinNumber(pin) ;
  pinMode(pin_number, INPUT) ;
  out->value = digitalRead(pin_number) ;
}

void Reacduino__digitalWrite_step(Reacduino__gpio_pin pin, bool value,
				  Reacduino__digitalWrite_out *out) {
  int pin_number = getGPIOPinNumber(pin) ;
  pinMode(pin_number, OUTPUT) ;
  digitalWrite(pin_number, value) ;
}

void Reacduino__pinMode_step(Reacduino__gpio_pin pin, Reacduino__io_mode mode,
			     Reacduino__pinMode_out *out) {
  pinMode(getGPIOPinNumber(pin), _getIOModeNumber(mode)) ;
}


int _getIOModeNumber(Reacduino__io_mode mode) {
  switch (mode) {
  case Reacduino__INPUT :
    return INPUT ;
  case Reacduino__OUTPUT :
    return OUTPUT ;
  case Reacduino__INPUT_PULLUP :
    return INPUT_PULLUP ;
  } ;
}
