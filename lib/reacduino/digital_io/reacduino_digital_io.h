#ifndef REACDUINO_DIGITAL_IO_H
#define REACDUINO_DIGITAL_IO_H

#include <stdbool.h>
#include "reacduino_digital_io_types.h"

void Reacduino__digitalRead_step(Reacduino__gpio_pin pin,
			      Reacduino__digitalRead_out *out) ;

void Reacduino__digitalWrite_step(Reacduino__gpio_pin pin, bool value,
			       Reacduino__digitalWrite_out *out) ;

void Reacduino__pinMode_step(Reacduino__gpio_pin pin, Reacduino__io_mode mode,
			     Reacduino__pinMode_out *out) ;

#endif
