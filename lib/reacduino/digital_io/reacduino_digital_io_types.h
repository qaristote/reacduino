#ifndef REACDUINO_DIGITAL_IO_TYPES_H
#define REACDUINO_DIGITAL_IO_TYPES_H

#include <stdbool.h>
#include "../reacduino_types.h"

typedef bool_out Reacduino__digitalRead_out ;

typedef unit_out Reacduino__digitalWrite_out ;

typedef unit_out Reacduino__pinMode_out ;

typedef enum {
	      Reacduino__INPUT,
	      Reacduino__OUTPUT,
	      Reacduino__INPUT_PULLUP
} Reacduino__io_mode ;

#endif
