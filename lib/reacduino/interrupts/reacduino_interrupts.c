#include <stdbool.h>
#include "Arduino.h"
#include "reacduino_interrupts.h"
#include "../macros/reacduino_macros.h"

void Reacduino__interrupts_step(Reacduino__interrupts_out *out) {
  interrupts() ;
}

void Reacduino__noInterrupts_step(Reacduino__noInterrupts_out *out) {
  noInterrupts() ;
}

#define defineISRs(pin_id, mode)					\
  volatile unsigned long _counter_ ## pin_id ## mode = 0 ;		\
  volatile unsigned long _last_time_ ## pin_id ## mode = 0 ;		\
  void _detect_ ## pin_id ## mode () {					\
    _last_time_ ## pin_id ## mode = micros() ;				\
    _counter_ ## pin_id ## mode ++ ;					\
  }

#define defineCountInterruptsFunctions(pin_id, pin_num, mode)		\
  void Reacduino__countInterrupts_params_ ## pin_id ## mode ## _reset(Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem *mem) { \
    mem->last_counter = _counter_ ## pin_id ## mode ;			\
    attachInterrupt(digitalPinToInterrupt(pin_num), _detect_ ## pin_id ## mode , mode) ; \
  }									\
  void Reacduino__countInterrupts_params_ ## pin_id ## mode ## _step(Reacduino__countInterrupts_params_ ## pin_id ## mode ## _out *out, \
								      Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem *mem) { \
    int new_counter = _counter_ ## pin_id ## mode ;			\
    out->value = (int) new_counter - mem->last_counter ;		\
    mem->last_counter = new_counter ;					\
  }

#define defineOnInterruptFunctions(pin_id, mode)			\
  void Reacduino__onInterrupt_params_ ## pin_id ## mode ## _reset(Reacduino__onInterrupt_params_ ## pin_id ## mode ## _mem *mem) { \
    Reacduino__countInterrupts_params_ ## pin_id ## mode ## _reset(&(mem->countInterrupts_mem)) ; \
  }									\
  void Reacduino__onInterrupt_params_ ## pin_id ## mode ## _step(Reacduino__onInterrupt_params_ ## pin_id ## mode ## _out *out, \
								 Reacduino__onInterrupt_params_ ## pin_id ## mode ## _mem *mem) { \
    Reacduino__countInterrupts_params_ ## pin_id ## mode ## _step(&(mem->countInterrupts_out), \
								    &(mem->countInterrupts_mem)) ; \
    out->value = (mem->countInterrupts_out).value > 0 ;			\
  }

#define defineLastInterruptFunctions(pin_id, pin_num, mode)		\
  void Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _reset(Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _mem *mem) { \
    attachInterrupt(digitalPinToInterrupt(pin_num), _detect_ ## pin_id ## mode , mode) ; \
  }									\
  void Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _step(Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _out *out, \
								      Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _mem *mem) { \
    out->value = (int) (_last_time_ ## pin_id ## mode / 1000) ;		\
  }

#define defineLastInterruptMicrosFunctions(pin_id, pin_num, mode)	\
  void Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _reset(Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _mem *mem) { \
    attachInterrupt(digitalPinToInterrupt(pin_num), _detect_ ## pin_id ## mode , mode) ; \
  }									\
  void Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _step(Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _out *out, \
									   Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _mem *mem) { \
    out->value = (int) _last_time_ ## pin_id ## mode ;			\
  }


#define defineInterruptsFunctionsForDigitalPin(pin)			\
  defineISRs(ISR_D ## pin , LOW)					\
    defineISRs(ISR_D ## pin , HIGH)					\
    defineISRs(ISR_D ## pin , FALLING)					\
    defineISRs(ISR_D ## pin , RISING)					\
    defineISRs(ISR_D ## pin , CHANGE)					\
    defineCountInterruptsFunctions(ISR_D ## pin , pin, LOW)		\
    defineCountInterruptsFunctions(ISR_D ## pin , pin, HIGH)		\
    defineCountInterruptsFunctions(ISR_D ## pin , pin, RISING)		\
    defineCountInterruptsFunctions(ISR_D ## pin , pin, FALLING)		\
    defineCountInterruptsFunctions(ISR_D ## pin , pin, CHANGE)		\
    defineOnInterruptFunctions(ISR_D ## pin , LOW)			\
    defineOnInterruptFunctions(ISR_D ## pin , HIGH)			\
    defineOnInterruptFunctions(ISR_D ## pin , RISING)			\
    defineOnInterruptFunctions(ISR_D ## pin , FALLING)			\
    defineOnInterruptFunctions(ISR_D ## pin , CHANGE)			\
    defineLastInterruptFunctions(ISR_D ## pin , pin, LOW)		\
    defineLastInterruptFunctions(ISR_D ## pin , pin, HIGH)		\
    defineLastInterruptFunctions(ISR_D ## pin , pin, RISING)		\
    defineLastInterruptFunctions(ISR_D ## pin , pin, FALLING)		\
    defineLastInterruptFunctions(ISR_D ## pin , pin, CHANGE)		\
    defineLastInterruptMicrosFunctions(ISR_D ## pin , pin, LOW)	\
    defineLastInterruptMicrosFunctions(ISR_D ## pin , pin, HIGH)	\
    defineLastInterruptMicrosFunctions(ISR_D ## pin , pin, RISING)	\
    defineLastInterruptMicrosFunctions(ISR_D ## pin , pin, FALLING)	\
    defineLastInterruptMicrosFunctions(ISR_D ## pin , pin, CHANGE)  
    
#define defineInterruptsFunctionsForAnalogInput(pin)			\
    defineISRs(ISR_ ## pin , LOW)					\
      defineISRs(ISR_ ## pin , HIGH)					\
      defineISRs(ISR_ ## pin , FALLING)					\
      defineISRs(ISR_ ## pin , RISING)					\
      defineISRs(ISR_ ## pin , CHANGE)					\
      defineCountInterruptsFunctions(ISR_ ## pin , pin, LOW)		\
      defineCountInterruptsFunctions(ISR_ ## pin , pin, HIGH)		\
      defineCountInterruptsFunctions(ISR_ ## pin , pin, RISING)		\
      defineCountInterruptsFunctions(ISR_ ## pin , pin, FALLING)	\
      defineCountInterruptsFunctions(ISR_ ## pin , pin, CHANGE)		\
      defineOnInterruptFunctions(ISR_ ## pin , LOW)			\
      defineOnInterruptFunctions(ISR_ ## pin , HIGH)			\
      defineOnInterruptFunctions(ISR_ ## pin , RISING)			\
      defineOnInterruptFunctions(ISR_ ## pin , FALLING)			\
      defineOnInterruptFunctions(ISR_ ## pin , CHANGE)			\
      defineLastInterruptFunctions(ISR_ ## pin , pin, LOW)		\
      defineLastInterruptFunctions(ISR_ ## pin , pin, HIGH)		\
      defineLastInterruptFunctions(ISR_ ## pin , pin, RISING)		\
      defineLastInterruptFunctions(ISR_ ## pin , pin, FALLING)		\
      defineLastInterruptFunctions(ISR_ ## pin , pin, CHANGE)		\
      defineLastInterruptMicrosFunctions(ISR_ ## pin , pin, LOW)	\
      defineLastInterruptMicrosFunctions(ISR_ ## pin , pin, HIGH)	\
      defineLastInterruptMicrosFunctions(ISR_ ## pin , pin, RISING)	\
      defineLastInterruptMicrosFunctions(ISR_ ## pin , pin, FALLING)	\
      defineLastInterruptMicrosFunctions(ISR_ ## pin , pin, CHANGE)	

    
#if digitalPinHasInterrupts(0)
    defineInterruptsFunctionsForDigitalPin(0)
#endif
#if digitalPinHasInterrupts(1)
    defineInterruptsFunctionsForDigitalPin(1)
#endif
#if digitalPinHasInterrupts(2)
    defineInterruptsFunctionsForDigitalPin(2)
#endif
#if digitalPinHasInterrupts(3)
    defineInterruptsFunctionsForDigitalPin(3)
#endif
#if digitalPinHasInterrupts(4)
      defineInterruptsFunctionsForDigitalPin(4)
#endif
#if digitalPinHasInterrupts(5)
    defineInterruptsFunctionsForDigitalPin(5)
#endif
#if digitalPinHasInterrupts(6)
    defineInterruptsFunctionsForDigitalPin(6)
#endif
#if digitalPinHasInterrupts(8)
    defineInterruptsFunctionsForDigitalPin(8)
#endif
#if digitalPinHasInterrupts(9)
    defineInterruptsFunctionsForDigitalPin(9)
#endif
#if digitalPinHasInterrupts(10)
    defineInterruptsFunctionsForDigitalPin(10)
#endif
#if digitalPinHasInterrupts(11)
    defineInterruptsFunctionsForDigitalPin(11)
#endif
#if digitalPinHasInterrupts(12)
    defineInterruptsFunctionsForDigitalPin(12)
#endif
#if digitalPinHasInterrupts(13)
    defineInterruptsFunctionsForDigitalPin(13)
#endif
#if digitalPinHasInterrupts(14)
    defineInterruptsFunctionsForDigitalPin(14)
#endif
#if digitalPinHasInterrupts(15)
    defineInterruptsFunctionsForDigitalPin(15)
#endif
#if digitalPinHasInterrupts(16)
    defineInterruptsFunctionsForDigitalPin(16)
#endif
#if digitalPinHasInterrupts(17)
    defineInterruptsFunctionsForDigitalPin(17)
#endif
#if digitalPinHasInterrupts(18)
    defineInterruptsFunctionsForDigitalPin(18)
#endif
#if digitalPinHasInterrupts(19)
    defineInterruptsFunctionsForDigitalPin(19)
#endif
#if digitalPinHasInterrupts(20)
    defineInterruptsFunctionsForDigitalPin(20)
#endif
#if digitalPinHasInterrupts(21)
    defineInterruptsFunctionsForDigitalPin(21)
#endif
#if digitalPinHasInterrupts(22)
    defineInterruptsFunctionsForDigitalPin(22)
#endif
#if digitalPinHasInterrupts(23)
    defineInterruptsFunctionsForDigitalPin(23)
#endif
#if digitalPinHasInterrupts(24)
    defineInterruptsFunctionsForDigitalPin(24)
#endif
#if digitalPinHasInterrupts(25)
    defineInterruptsFunctionsForDigitalPin(25)
#endif
#if digitalPinHasInterrupts(26)
    defineInterruptsFunctionsForDigitalPin(26)
#endif
#if digitalPinHasInterrupts(27)
    defineInterruptsFunctionsForDigitalPin(27)
#endif
#if digitalPinHasInterrupts(28)
    defineInterruptsFunctionsForDigitalPin(28)
#endif
#if digitalPinHasInterrupts(29)
    defineInterruptsFunctionsForDigitalPin(29)
#endif
#if digitalPinHasInterrupts(30)
    defineInterruptsFunctionsForDigitalPin(30)
#endif
#if digitalPinHasInterrupts(31)
    defineInterruptsFunctionsForDigitalPin(31)
#endif
#if digitalPinHasInterrupts(32)
    defineInterruptsFunctionsForDigitalPin(32)
#endif
#if digitalPinHasInterrupts(33)
    defineInterruptsFunctionsForDigitalPin(33)
#endif
#if digitalPinHasInterrupts(34)
    defineInterruptsFunctionsForDigitalPin(34)
#endif
#if digitalPinHasInterrupts(35)
    defineInterruptsFunctionsForDigitalPin(35)
#endif
#if digitalPinHasInterrupts(36)
    defineInterruptsFunctionsForDigitalPin(36)
#endif
#if digitalPinHasInterrupts(37)
    defineInterruptsFunctionsForDigitalPin(37)
#endif
#if digitalPinHasInterrupts(38)
    defineInterruptsFunctionsForDigitalPin(38)
#endif
#if digitalPinHasInterrupts(39)
    defineInterruptsFunctionsForDigitalPin(39)
#endif
#if digitalPinHasInterrupts(40)
    defineInterruptsFunctionsForDigitalPin(40)
#endif
#if digitalPinHasInterrupts(41)
    defineInterruptsFunctionsForDigitalPin(41)
#endif
#if digitalPinHasInterrupts(42)
    defineInterruptsFunctionsForDigitalPin(42)
#endif
#if digitalPinHasInterrupts(43)
    defineInterruptsFunctionsForDigitalPin(43)
#endif
#if digitalPinHasInterrupts(44)
    defineInterruptsFunctionsForDigitalPin(44)
#endif
#if digitalPinHasInterrupts(45)
    defineInterruptsFunctionsForDigitalPin(45)
#endif
#if digitalPinHasInterrupts(46)
    defineInterruptsFunctionsForDigitalPin(46)
#endif
#if digitalPinHasInterrupts(47)
    defineInterruptsFunctionsForDigitalPin(47)
#endif
#if digitalPinHasInterrupts(48)
    defineInterruptsFunctionsForDigitalPin(48)
#endif
#if digitalPinHasInterrupts(49)
    defineInterruptsFunctionsForDigitalPin(49)
#endif
#if digitalPinHasInterrupts(50)
    defineInterruptsFunctionsForDigitalPin(50)
#endif
#if digitalPinHasInterrupts(51)
    defineInterruptsFunctionsForDigitalPin(51)
#endif
#if digitalPinHasInterrupts(52)
    defineInterruptsFunctionsForDigitalPin(52)
#endif
#if digitalPinHasInterrupts(53)
    defineInterruptsFunctionsForDigitalPin(53)
#endif
#if analogInputHasInterrupts(A0)
    defineInterruptsFunctionsForAnalogInput(A0)
#endif
#if analogInputHasInterrupts(A1)
    defineInterruptsFunctionsForAnalogInput(A1)
#endif
#if analogInputHasInterrupts(A2)
    defineInterruptsFunctionsForAnalogInput(A2)
#endif
#if analogInputHasInterrupts(A3)
    defineInterruptsFunctionsForAnalogInput(A3)
#endif
#if analogInputHasInterrupts(A4)
    defineInterruptsFunctionsForAnalogInput(A4)
#endif
#if analogInputHasInterrupts(A5)
    defineInterruptsFunctionsForAnalogInput(A5)
#endif
#if analogInputHasInterrupts(A6)
    defineInterruptsFunctionsForAnalogInput(A6)
#endif
#if analogInputHasInterrupts(A7)
    defineInterruptsFunctionsForAnalogInput(A7)
#endif
#if analogInputHasInterrupts(A8)
    defineInterruptsFunctionsForAnalogInput(A8)
#endif
#if analogInputHasInterrupts(A9)
    defineInterruptsFunctionsForAnalogInput(A9)
#endif
#if analogInputHasInterrupts(A10)
    defineInterruptsFunctionsForAnalogInput(A10)
#endif
#if analogInputHasInterrupts(A11)
    defineInterruptsFunctionsForAnalogInput(A11)
#endif
#if analogInputHasInterrupts(A12)
    defineInterruptsFunctionsForAnalogInput(A12)
#endif
#if analogInputHasInterrupts(A13)
    defineInterruptsFunctionsForAnalogInput(A13)
#endif
#if analogInputHasInterrupts(A14)
    defineInterruptsFunctionsForAnalogInput(A14)
#endif
#if analogInputHasInterrupts(A15)
    defineInterruptsFunctionsForAnalogInput(A15)
#endif
