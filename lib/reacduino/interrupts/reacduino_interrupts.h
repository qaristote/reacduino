#ifndef REACDUINO_INTERRUPTS_H
#define REACDUINO_INTERRUPTS_H

#include "reacduino_interrupts_types.h"
#include "../macros/reacduino_macros.h"

void Reacduino__interrupts_step(Reacduino__interrupts_out *out) ;

void Reacduino__noInterrupts_step(Reacduino__noInterrupts_out *out) ;

#define defineCountInterruptsSignatures(pin_id, mode)			\
  void Reacduino__countInterrupts_params_ ## pin_id ## mode ## _reset(Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem *mem) ; \
  void Reacduino__countInterrupts_params_ ## pin_id ## mode ## _step(Reacduino__countInterrupts_params_ ## pin_id ## mode ## _out *out, \
								   Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem *mem) ;					

#define defineOnInterruptSignatures(pin_id, mode)			\
  void Reacduino__onInterrupt_params_ ## pin_id ## mode ## _reset(Reacduino__onInterrupt_params_ ## pin_id ## mode ## _mem *mem) ; \
  void Reacduino__onInterrupt_params_ ## pin_id ## mode ## _step(Reacduino__onInterrupt_params_ ## pin_id ## mode ## _out *out, \
								 Reacduino__onInterrupt_params_ ## pin_id ## mode ## _mem *mem) ;

#define defineLastInterruptSignatures(pin_id, mode)			\
  void Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _reset(Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _mem *mem) ; \
  void Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _step(Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _out *out, \
								   Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _mem *mem) ;					

#define defineLastInterruptMicrosSignatures(pin_id, mode)			\
  void Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _reset(Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _mem *mem) ; \
  void Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _step(Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _out *out, \
								   Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _mem *mem) ;


#define defineInterruptsSignaturesForDigitalPin(pin)			\
  defineCountInterruptsSignatures(ISR_D ## pin , LOW)			\
    defineCountInterruptsSignatures(ISR_D ## pin , HIGH)		\
    defineCountInterruptsSignatures(ISR_D ## pin , RISING)		\
    defineCountInterruptsSignatures(ISR_D ## pin , FALLING)		\
    defineCountInterruptsSignatures(ISR_D ## pin , CHANGE)		\
    defineOnInterruptSignatures(ISR_D ## pin , LOW)			\
    defineOnInterruptSignatures(ISR_D ## pin , HIGH)			\
    defineOnInterruptSignatures(ISR_D ## pin , RISING)			\
    defineOnInterruptSignatures(ISR_D ## pin , FALLING)			\
    defineOnInterruptSignatures(ISR_D ## pin , CHANGE)			\
    defineLastInterruptSignatures(ISR_D ## pin , LOW)			\
    defineLastInterruptSignatures(ISR_D ## pin , HIGH)			\
    defineLastInterruptSignatures(ISR_D ## pin , RISING)		\
    defineLastInterruptSignatures(ISR_D ## pin , FALLING)		\
    defineLastInterruptSignatures(ISR_D ## pin , CHANGE)		\
    defineLastInterruptMicrosSignatures(ISR_D ## pin , LOW)		\
    defineLastInterruptMicrosSignatures(ISR_D ## pin , HIGH)		\
    defineLastInterruptMicrosSignatures(ISR_D ## pin , RISING)		\
    defineLastInterruptMicrosSignatures(ISR_D ## pin , FALLING)	\
    defineLastInterruptMicrosSignatures(ISR_D ## pin , CHANGE)
    
#define defineInterruptsSignaturesForAnalogInput(pin)		\
  defineCountInterruptsSignatures(ISR_ ## pin , LOW)		\
    defineCountInterruptsSignatures(ISR_ ## pin , HIGH)		\
    defineCountInterruptsSignatures(ISR_ ## pin , RISING)	\
    defineCountInterruptsSignatures(ISR_ ## pin , FALLING)	\
    defineCountInterruptsSignatures(ISR_ ## pin , CHANGE)	\
    defineOnInterruptSignatures(ISR_ ## pin , LOW)		\
    defineOnInterruptSignatures(ISR_ ## pin , HIGH)		\
    defineOnInterruptSignatures(ISR_ ## pin , RISING)		\
    defineOnInterruptSignatures(ISR_ ## pin , FALLING)		\
    defineOnInterruptSignatures(ISR_ ## pin , CHANGE)		\
    defineLastInterruptSignatures(ISR_ ## pin , LOW)		\
    defineLastInterruptSignatures(ISR_ ## pin , HIGH)		\
    defineLastInterruptSignatures(ISR_ ## pin , RISING)	\
    defineLastInterruptSignatures(ISR_ ## pin , FALLING)	\
    defineLastInterruptSignatures(ISR_ ## pin , CHANGE)	\
    defineLastInterruptMicrosSignatures(ISR_ ## pin , LOW)	\
    defineLastInterruptMicrosSignatures(ISR_ ## pin , HIGH)	\
    defineLastInterruptMicrosSignatures(ISR_ ## pin , RISING)	\
    defineLastInterruptMicrosSignatures(ISR_ ## pin , FALLING)	\
    defineLastInterruptMicrosSignatures(ISR_ ## pin , CHANGE)

    
#if digitalPinHasInterrupts(0)
    defineInterruptsSignaturesForDigitalPin(0)
#endif
#if digitalPinHasInterrupts(1)
      defineInterruptsSignaturesForDigitalPin(1)
#endif
#if digitalPinHasInterrupts(2)
    defineInterruptsSignaturesForDigitalPin(2)
#endif
#if digitalPinHasInterrupts(3)
      defineInterruptsSignaturesForDigitalPin(3)
#endif
#if digitalPinHasInterrupts(4)
      defineInterruptsSignaturesForDigitalPin(4)
#endif
#if digitalPinHasInterrupts(5)
      defineInterruptsSignaturesForDigitalPin(5)
#endif
#if digitalPinHasInterrupts(6)
    defineInterruptsSignaturesForDigitalPin(6)
#endif
#if digitalPinHasInterrupts(8)
    defineInterruptsSignaturesForDigitalPin(8)
#endif
#if digitalPinHasInterrupts(9)
    defineInterruptsSignaturesForDigitalPin(9)
#endif
#if digitalPinHasInterrupts(10)
    defineInterruptsSignaturesForDigitalPin(10)
#endif
#if digitalPinHasInterrupts(11)
    defineInterruptsSignaturesForDigitalPin(11)
#endif
#if digitalPinHasInterrupts(12)
    defineInterruptsSignaturesForDigitalPin(12)
#endif
#if digitalPinHasInterrupts(13)
    defineInterruptsSignaturesForDigitalPin(13)
#endif
#if digitalPinHasInterrupts(14)
    defineInterruptsSignaturesForDigitalPin(14)
#endif
#if digitalPinHasInterrupts(15)
    defineInterruptsSignaturesForDigitalPin(15)
#endif
#if digitalPinHasInterrupts(16)
    defineInterruptsSignaturesForDigitalPin(16)
#endif
#if digitalPinHasInterrupts(17)
    defineInterruptsSignaturesForDigitalPin(17)
#endif
#if digitalPinHasInterrupts(18)
    defineInterruptsSignaturesForDigitalPin(18)
#endif
#if digitalPinHasInterrupts(19)
    defineInterruptsSignaturesForDigitalPin(19)
#endif
#if digitalPinHasInterrupts(20)
    defineInterruptsSignaturesForDigitalPin(20)
#endif
#if digitalPinHasInterrupts(21)
    defineInterruptsSignaturesForDigitalPin(21)
#endif
#if digitalPinHasInterrupts(22)
    defineInterruptsSignaturesForDigitalPin(22)
#endif
#if digitalPinHasInterrupts(23)
    defineInterruptsSignaturesForDigitalPin(23)
#endif
#if digitalPinHasInterrupts(24)
    defineInterruptsSignaturesForDigitalPin(24)
#endif
#if digitalPinHasInterrupts(25)
    defineInterruptsSignaturesForDigitalPin(25)
#endif
#if digitalPinHasInterrupts(26)
    defineInterruptsSignaturesForDigitalPin(26)
#endif
#if digitalPinHasInterrupts(27)
    defineInterruptsSignaturesForDigitalPin(27)
#endif
#if digitalPinHasInterrupts(28)
    defineInterruptsSignaturesForDigitalPin(28)
#endif
#if digitalPinHasInterrupts(29)
    defineInterruptsSignaturesForDigitalPin(29)
#endif
#if digitalPinHasInterrupts(30)
    defineInterruptsSignaturesForDigitalPin(30)
#endif
#if digitalPinHasInterrupts(31)
    defineInterruptsSignaturesForDigitalPin(31)
#endif
#if digitalPinHasInterrupts(32)
    defineInterruptsSignaturesForDigitalPin(32)
#endif
#if digitalPinHasInterrupts(33)
    defineInterruptsSignaturesForDigitalPin(33)
#endif
#if digitalPinHasInterrupts(34)
    defineInterruptsSignaturesForDigitalPin(34)
#endif
#if digitalPinHasInterrupts(35)
    defineInterruptsSignaturesForDigitalPin(35)
#endif
#if digitalPinHasInterrupts(36)
    defineInterruptsSignaturesForDigitalPin(36)
#endif
#if digitalPinHasInterrupts(37)
    defineInterruptsSignaturesForDigitalPin(37)
#endif
#if digitalPinHasInterrupts(38)
    defineInterruptsSignaturesForDigitalPin(38)
#endif
#if digitalPinHasInterrupts(39)
    defineInterruptsSignaturesForDigitalPin(39)
#endif
#if digitalPinHasInterrupts(40)
    defineInterruptsSignaturesForDigitalPin(40)
#endif
#if digitalPinHasInterrupts(41)
    defineInterruptsSignaturesForDigitalPin(41)
#endif
#if digitalPinHasInterrupts(42)
    defineInterruptsSignaturesForDigitalPin(42)
#endif
#if digitalPinHasInterrupts(43)
    defineInterruptsSignaturesForDigitalPin(43)
#endif
#if digitalPinHasInterrupts(44)
    defineInterruptsSignaturesForDigitalPin(44)
#endif
#if digitalPinHasInterrupts(45)
    defineInterruptsSignaturesForDigitalPin(45)
#endif
#if digitalPinHasInterrupts(46)
    defineInterruptsSignaturesForDigitalPin(46)
#endif
#if digitalPinHasInterrupts(47)
    defineInterruptsSignaturesForDigitalPin(47)
#endif
#if digitalPinHasInterrupts(48)
    defineInterruptsSignaturesForDigitalPin(48)
#endif
#if digitalPinHasInterrupts(49)
    defineInterruptsSignaturesForDigitalPin(49)
#endif
#if digitalPinHasInterrupts(50)
    defineInterruptsSignaturesForDigitalPin(50)
#endif
#if digitalPinHasInterrupts(51)
    defineInterruptsSignaturesForDigitalPin(51)
#endif
#if digitalPinHasInterrupts(52)
    defineInterruptsSignaturesForDigitalPin(52)
#endif
#if digitalPinHasInterrupts(53)
    defineInterruptsSignaturesForDigitalPin(53)
#endif
#if analogInputHasInterrupts(A0)
    defineInterruptsSignaturesForAnalogInput(A0)
#endif
#if analogInputHasInterrupts(A1)
    defineInterruptsSignaturesForAnalogInput(A1)
#endif
#if analogInputHasInterrupts(A2)
    defineInterruptsSignaturesForAnalogInput(A2)
#endif
#if analogInputHasInterrupts(A3)
    defineInterruptsSignaturesForAnalogInput(A3)
#endif
#if analogInputHasInterrupts(A4)
    defineInterruptsSignaturesForAnalogInput(A4)
#endif
#if analogInputHasInterrupts(A5)
    defineInterruptsSignaturesForAnalogInput(A5)
#endif
#if analogInputHasInterrupts(A6)
    defineInterruptsSignaturesForAnalogInput(A6)
#endif
#if analogInputHasInterrupts(A7)
    defineInterruptsSignaturesForAnalogInput(A7)
#endif
#if analogInputHasInterrupts(A8)
    defineInterruptsSignaturesForAnalogInput(A8)
#endif
#if analogInputHasInterrupts(A9)
    defineInterruptsSignaturesForAnalogInput(A9)
#endif
#if analogInputHasInterrupts(A10)
    defineInterruptsSignaturesForAnalogInput(A10)
#endif
#if analogInputHasInterrupts(A11)
    defineInterruptsSignaturesForAnalogInput(A11)
#endif
#if analogInputHasInterrupts(A12)
    defineInterruptsSignaturesForAnalogInput(A12)
#endif
#if analogInputHasInterrupts(A13)
    defineInterruptsSignaturesForAnalogInput(A13)
#endif
#if analogInputHasInterrupts(A14)
    defineInterruptsSignaturesForAnalogInput(A14)
#endif
#if analogInputHasInterrupts(A15)
    defineInterruptsSignaturesForAnalogInput(A15)
#endif

#endif
