#ifndef REACDUINO_INTERRUPTS_TYPES_H
#define REACDUINO_INTERRUPTS_TYPES_H

#include "../macros/reacduino_macros.h"
#include "../reacduino_types.h"

typedef unit_out Reacduino__interrupts_out ;

typedef unit_out Reacduino__noInterrupts_out ;

typedef enum {
	      Reacduino__LOW,
	      Reacduino__HIGH,
	      Reacduino__RISING,
	      Reacduino__FALLING,
	      Reacduino__CHANGE
} Reacduino__isr_mode ;

#define defineCountInterruptsTypes(pin_id, mode)			\
  typedef struct Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem { \
    int last_counter ;							\
  } Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem ;	\
  typedef int_out Reacduino__countInterrupts_params_ ## pin_id ## mode ## _out ;

#define defineOnInterruptTypes(pin_id, mode)				\
  typedef struct Reacduino__onInterrupt_params_ ## pin_id ## mode ## _mem { \
    Reacduino__countInterrupts_params_ ## pin_id ## mode ## _mem countInterrupts_mem ; \
    Reacduino__countInterrupts_params_ ## pin_id ## mode ## _out countInterrupts_out ;	\
  } Reacduino__onInterrupt_params_ ## pin_id ## mode ## _mem ;		\
  typedef bool_out Reacduino__onInterrupt_params_ ## pin_id ## mode ## _out ;

#define defineLastInterruptTypes(pin_id, mode)				\
  typedef empty_mem Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _mem ; \
  typedef int_out Reacduino__lastInterrupt_params_ ## pin_id ## mode ## _out ;

#define defineLastInterruptMicrosTypes(pin_id, mode)				\
  typedef empty_mem Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _mem ; \
  typedef int_out Reacduino__lastInterruptMicros_params_ ## pin_id ## mode ## _out ;

  
#define defineInterruptsTypesForDigitalPin(pin)			\
  defineCountInterruptsTypes(ISR_D ## pin , LOW)		\
    defineCountInterruptsTypes(ISR_D ## pin , HIGH)		\
    defineCountInterruptsTypes(ISR_D ## pin , RISING)		\
    defineCountInterruptsTypes(ISR_D ## pin , FALLING)		\
    defineCountInterruptsTypes(ISR_D ## pin , CHANGE)		\
    defineOnInterruptTypes(ISR_D ## pin , LOW)			\
    defineOnInterruptTypes(ISR_D ## pin , HIGH)			\
    defineOnInterruptTypes(ISR_D ## pin , RISING)		\
    defineOnInterruptTypes(ISR_D ## pin , FALLING)		\
    defineOnInterruptTypes(ISR_D ## pin , CHANGE)		\
    defineLastInterruptTypes(ISR_D ## pin , LOW)		\
    defineLastInterruptTypes(ISR_D ## pin , HIGH)		\
    defineLastInterruptTypes(ISR_D ## pin , RISING)		\
    defineLastInterruptTypes(ISR_D ## pin , FALLING)		\
    defineLastInterruptTypes(ISR_D ## pin , CHANGE)		\
    defineLastInterruptMicrosTypes(ISR_D ## pin , LOW)		\
    defineLastInterruptMicrosTypes(ISR_D ## pin , HIGH)	\
    defineLastInterruptMicrosTypes(ISR_D ## pin , RISING)	\
    defineLastInterruptMicrosTypes(ISR_D ## pin , FALLING)	\
    defineLastInterruptMicrosTypes(ISR_D ## pin , CHANGE)


#define defineInterruptsTypesForAnalogInput(pin)		\
  defineCountInterruptsTypes(ISR_ ## pin , LOW)			\
    defineCountInterruptsTypes(ISR_ ## pin , HIGH)		\
    defineCountInterruptsTypes(ISR_ ## pin , RISING)		\
    defineCountInterruptsTypes(ISR_ ## pin , FALLING)		\
    defineCountInterruptsTypes(ISR_ ## pin , CHANGE)		\
    defineOnInterruptTypes(ISR_ ## pin , LOW)			\
    defineOnInterruptTypes(ISR_ ## pin , HIGH)			\
    defineOnInterruptTypes(ISR_ ## pin , RISING)		\
    defineOnInterruptTypes(ISR_ ## pin , FALLING)		\
    defineOnInterruptTypes(ISR_ ## pin , CHANGE)		\
    defineLastInterruptTypes(ISR_ ## pin , LOW)		\
    defineLastInterruptTypes(ISR_ ## pin , HIGH)		\
    defineLastInterruptTypes(ISR_ ## pin , RISING)		\
    defineLastInterruptTypes(ISR_ ## pin , FALLING)		\
    defineLastInterruptTypes(ISR_ ## pin , CHANGE)		\
    defineLastInterruptMicrosTypes(ISR_ ## pin , LOW)		\
    defineLastInterruptMicrosTypes(ISR_ ## pin , HIGH)		\
    defineLastInterruptMicrosTypes(ISR_ ## pin , RISING)	\
    defineLastInterruptMicrosTypes(ISR_ ## pin , FALLING)	\
    defineLastInterruptMicrosTypes(ISR_ ## pin , CHANGE)
  

    
#if digitalPinHasInterrupts(0)
    defineInterruptsTypesForDigitalPin(0)
#endif
#if digitalPinHasInterrupts(1)
      defineInterruptsTypesForDigitalPin(1)
#endif
#if digitalPinHasInterrupts(2)
    defineInterruptsTypesForDigitalPin(2)
#endif
#if digitalPinHasInterrupts(3)
      defineInterruptsTypesForDigitalPin(3)
#endif
#if digitalPinHasInterrupts(4)
      defineInterruptsTypesForDigitalPin(4)
#endif
#if digitalPinHasInterrupts(5)
      defineInterruptsTypesForDigitalPin(5)
#endif
#if digitalPinHasInterrupts(6)
    defineInterruptsTypesForDigitalPin(6)
#endif
#if digitalPinHasInterrupts(8)
    defineInterruptsTypesForDigitalPin(8)
#endif
#if digitalPinHasInterrupts(9)
    defineInterruptsTypesForDigitalPin(9)
#endif
#if digitalPinHasInterrupts(10)
    defineInterruptsTypesForDigitalPin(10)
#endif
#if digitalPinHasInterrupts(11)
    defineInterruptsTypesForDigitalPin(11)
#endif
#if digitalPinHasInterrupts(12)
    defineInterruptsTypesForDigitalPin(12)
#endif
#if digitalPinHasInterrupts(13)
    defineInterruptsTypesForDigitalPin(13)
#endif
#if digitalPinHasInterrupts(14)
    defineInterruptsTypesForDigitalPin(14)
#endif
#if digitalPinHasInterrupts(15)
    defineInterruptsTypesForDigitalPin(15)
#endif
#if digitalPinHasInterrupts(16)
    defineInterruptsTypesForDigitalPin(16)
#endif
#if digitalPinHasInterrupts(17)
    defineInterruptsTypesForDigitalPin(17)
#endif
#if digitalPinHasInterrupts(18)
    defineInterruptsTypesForDigitalPin(18)
#endif
#if digitalPinHasInterrupts(19)
    defineInterruptsTypesForDigitalPin(19)
#endif
#if digitalPinHasInterrupts(20)
    defineInterruptsTypesForDigitalPin(20)
#endif
#if digitalPinHasInterrupts(21)
    defineInterruptsTypesForDigitalPin(21)
#endif
#if digitalPinHasInterrupts(22)
    defineInterruptsTypesForDigitalPin(22)
#endif
#if digitalPinHasInterrupts(23)
    defineInterruptsTypesForDigitalPin(23)
#endif
#if digitalPinHasInterrupts(24)
    defineInterruptsTypesForDigitalPin(24)
#endif
#if digitalPinHasInterrupts(25)
    defineInterruptsTypesForDigitalPin(25)
#endif
#if digitalPinHasInterrupts(26)
    defineInterruptsTypesForDigitalPin(26)
#endif
#if digitalPinHasInterrupts(27)
    defineInterruptsTypesForDigitalPin(27)
#endif
#if digitalPinHasInterrupts(28)
    defineInterruptsTypesForDigitalPin(28)
#endif
#if digitalPinHasInterrupts(29)
    defineInterruptsTypesForDigitalPin(29)
#endif
#if digitalPinHasInterrupts(30)
    defineInterruptsTypesForDigitalPin(30)
#endif
#if digitalPinHasInterrupts(31)
    defineInterruptsTypesForDigitalPin(31)
#endif
#if digitalPinHasInterrupts(32)
    defineInterruptsTypesForDigitalPin(32)
#endif
#if digitalPinHasInterrupts(33)
    defineInterruptsTypesForDigitalPin(33)
#endif
#if digitalPinHasInterrupts(34)
    defineInterruptsTypesForDigitalPin(34)
#endif
#if digitalPinHasInterrupts(35)
    defineInterruptsTypesForDigitalPin(35)
#endif
#if digitalPinHasInterrupts(36)
    defineInterruptsTypesForDigitalPin(36)
#endif
#if digitalPinHasInterrupts(37)
    defineInterruptsTypesForDigitalPin(37)
#endif
#if digitalPinHasInterrupts(38)
    defineInterruptsTypesForDigitalPin(38)
#endif
#if digitalPinHasInterrupts(39)
    defineInterruptsTypesForDigitalPin(39)
#endif
#if digitalPinHasInterrupts(40)
    defineInterruptsTypesForDigitalPin(40)
#endif
#if digitalPinHasInterrupts(41)
    defineInterruptsTypesForDigitalPin(41)
#endif
#if digitalPinHasInterrupts(42)
    defineInterruptsTypesForDigitalPin(42)
#endif
#if digitalPinHasInterrupts(43)
    defineInterruptsTypesForDigitalPin(43)
#endif
#if digitalPinHasInterrupts(44)
    defineInterruptsTypesForDigitalPin(44)
#endif
#if digitalPinHasInterrupts(45)
    defineInterruptsTypesForDigitalPin(45)
#endif
#if digitalPinHasInterrupts(46)
    defineInterruptsTypesForDigitalPin(46)
#endif
#if digitalPinHasInterrupts(47)
    defineInterruptsTypesForDigitalPin(47)
#endif
#if digitalPinHasInterrupts(48)
    defineInterruptsTypesForDigitalPin(48)
#endif
#if digitalPinHasInterrupts(49)
    defineInterruptsTypesForDigitalPin(49)
#endif
#if digitalPinHasInterrupts(50)
    defineInterruptsTypesForDigitalPin(50)
#endif
#if digitalPinHasInterrupts(51)
    defineInterruptsTypesForDigitalPin(51)
#endif
#if digitalPinHasInterrupts(52)
    defineInterruptsTypesForDigitalPin(52)
#endif
#if digitalPinHasInterrupts(53)
    defineInterruptsTypesForDigitalPin(53)
#endif
#if analogInputHasInterrupts(A0)
    defineInterruptsTypesForAnalogInput(A0)
#endif
#if analogInputHasInterrupts(A1)
    defineInterruptsTypesForAnalogInput(A1)
#endif
#if analogInputHasInterrupts(A2)
    defineInterruptsTypesForAnalogInput(A2)
#endif
#if analogInputHasInterrupts(A3)
    defineInterruptsTypesForAnalogInput(A3)
#endif
#if analogInputHasInterrupts(A4)
    defineInterruptsTypesForAnalogInput(A4)
#endif
#if analogInputHasInterrupts(A5)
    defineInterruptsTypesForAnalogInput(A5)
#endif
#if analogInputHasInterrupts(A6)
    defineInterruptsTypesForAnalogInput(A6)
#endif
#if analogInputHasInterrupts(A7)
    defineInterruptsTypesForAnalogInput(A7)
#endif
#if analogInputHasInterrupts(A8)
    defineInterruptsTypesForAnalogInput(A8)
#endif
#if analogInputHasInterrupts(A9)
    defineInterruptsTypesForAnalogInput(A9)
#endif
#if analogInputHasInterrupts(A10)
    defineInterruptsTypesForAnalogInput(A10)
#endif
#if analogInputHasInterrupts(A11)
    defineInterruptsTypesForAnalogInput(A11)
#endif
#if analogInputHasInterrupts(A12)
    defineInterruptsTypesForAnalogInput(A12)
#endif
#if analogInputHasInterrupts(A13)
    defineInterruptsTypesForAnalogInput(A13)
#endif
#if analogInputHasInterrupts(A14)
    defineInterruptsTypesForAnalogInput(A14)
#endif
#if analogInputHasInterrupts(A15)
    defineInterruptsTypesForAnalogInput(A15)
#endif

#endif
