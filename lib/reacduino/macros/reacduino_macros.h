#ifndef REACDUINO_MACROS_H
#define REACDUINO_MACROS_H

#include "Arduino.h"
#include "pins_arduino.h"

#define NUM_ONLY_DIGITAL_PINS (NUM_DIGITAL_PINS - NUM_ANALOG_INPUTS)

#define analogInputHasPWM(p) digitalPinHasPWM(analogInputToDigitalPin(p))
#define digitalPinIsAvailable(p) (NUM_ONLY_DIGITAL_PINS > p)
#define analogInputIsAvailable(p) (NUM_ANALOG_INPUTS > p)

// only for Arduino Uno yet
#define digitalPinHasInterrupts(p) (p == 2 || p == 3) 
#define analogInputHasInterrupts(p) (0)
#ifndef digitalPinToInterrupt(p)
#define digitalPinToInterrupt(p) ((p==2)?0:((p==3)?1:-1))
#endif

#endif
