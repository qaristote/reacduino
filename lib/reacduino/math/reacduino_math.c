#include "Arduino.h"
#include "reacduino_math.h"

// TODO : ensure no double -> long -> float -> int conversion problem can happen

/* Integer math */
void Reacduino__intOfFloat_step(float x,
				Reacduino__intOfFloat_out *out) {
  out->value = (int) x ;
}

void Reacduino__absInt_step(int n,
			    Reacduino__absInt_out *out) {
  out->value = abs(n) ;
}

void Reacduino__constrainInt_step(int n, int a, int b,
				  Reacduino__constrainInt_out *out) {
  out->value = constrain(n, a, b) ;
}

void Reacduino__mapInt_step(int n,
			    int from_low, int from_high,
			    int to_low, int to_high,
			    Reacduino__mapInt_out *out) {
  out->value = map(n, from_low, from_high, to_low, to_high) ;
}

void Reacduino__maxInt_step(int m, int n,
			    Reacduino__maxInt_out *out) {
  out->value = max(m,n) ;
}

void Reacduino__minInt_step(int m, int n,
			    Reacduino__minInt_out *out) {
  out->value = min(m,n) ;
}

void Reacduino__powInt_step(int n, int p,
			    Reacduino__powInt_out *out) {
  if (p < 0) {
    if (n == 1) {
      out->value = 1 ;
    } else {
      out->value = 0 ;
    } ;
  } else {
    Reacduino__powInt_step(n, p / 2, out) ;
    out->value = out->value * out->value ;
    if (p % 2 == 1) {
      out->value = out->value * n ;
    } ;
  } ;
}

void Reacduino__sqInt_step(int n,
			   Reacduino__sqInt_out *out) {
  out->value = n * n ;
}

/* Floating-point math */
void Reacduino__floatOfInt_step(int n,
				Reacduino__floatOfInt_out *out) {
  out->value = (float) n ;
}

void Reacduino__absFloat_step(float x,
			      Reacduino__absFloat_out *out) {
  out->value = abs(x) ;
}

void Reacduino__constrainFloat_step(float x, float a, float b,
				    Reacduino__constrainFloat_out *out) {
  out->value = constrain(x, a, b) ;
}

void Reacduino__mapFloat_step(float x,
			      float from_low, float from_high,
			      float to_low, float to_high,
			      Reacduino__mapFloat_out *out) {
  out->value = map(x, from_low, from_high, to_low, to_high) ;
}

void Reacduino__maxFloat_step(float x, float y,
			      Reacduino__maxFloat_out *out) {
  out->value = max(x,y) ;
}

void Reacduino__minFloat_step(float x, float y,
			      Reacduino__minFloat_out *out) {
  out->value = min(x,y) ;
}

void Reacduino__powFloat_step(float x, float p,
			      Reacduino__powFloat_out *out) {
  out->value = (float) pow(x,p) ;
}

void Reacduino__sqFloat_step(float x,
			     Reacduino__sqFloat_out *out) {
  out->value = (float) sq(x) ;
}

void Reacduino__sqrt_step(float x,
			  Reacduino__sqrt_out *out) {
  out->value = (float) sqrt(x) ;
}

void Reacduino__cos_step(float x,
			 Reacduino__cos_out *out) {
  out->value = (float) cos(x) ;
}

void Reacduino__sin_step(float x,
			 Reacduino__sin_out *out) {
  out->value = (float) sin(x) ;
}

void Reacduino__tan_step(float x,
			 Reacduino__tan_out *out) {
  out->value = (float) tan(x) ;
}
