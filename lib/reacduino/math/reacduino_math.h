#ifndef REACDUINO_MATH_H
#define REACDUINO_MATH_H

#include "reacduino_math_types.h"

/* Integer math */
void Reacduino__intOfFloat_step(float x,
				Reacduino__absInt_out *out) ;

void Reacduino__absInt_step(int n,
			    Reacduino__absInt_out *out) ;

void Reacduino__constrainInt_step(int n, int a, int b,
				  Reacduino__constrainInt_out *out) ;

void Reacduino__mapInt_step(int n,
			    int from_low, int from_high,
			    int to_low, int to_high,
			    Reacduino__mapInt_out *out) ;

void Reacduino__maxInt_step(int m, int n,
			    Reacduino__maxInt_out *out) ;

void Reacduino__minInt_step(int m, int n,
			    Reacduino__minInt_out *out) ;

void Reacduino__powInt_step(int n, int p,
			    Reacduino__powInt_out *out) ;

void Reacduino__sqInt_step(int n,
			   Reacduino__sqInt_out *out) ;

/* Floating-point math */
void Reacduino__floatOfInt_step(int n,
				Reacduino__floatOfInt_out *out) ;

void Reacduino__absFloat_step(float x,
			      Reacduino__absFloat_out *out) ;

void Reacduino__constrainFloat_step(float x, float a, float b,
				    Reacduino__constrainFloat_out *out) ;

void Reacduino__mapFloat_step(float x,
			      float from_low, float from_high,
			      float to_low, float to_high,
			      Reacduino__mapFloat_out *out) ;

void Reacduino__maxFloat_step(float x, float y,
			      Reacduino__maxFloat_out *out) ;

void Reacduino__minFloat_step(float x, float y,
			      Reacduino__minFloat_out *out) ;

void Reacduino__powFloat_step(float x, float p,
			      Reacduino__powFloat_out *out) ;

void Reacduino__sqFloat_step(float x,
			     Reacduino__sqFloat_out *out) ;

void Reacduino__sqrt_step(float x,
			  Reacduino__sqrt_out *out) ;

void Reacduino__cos_step(float x,
			 Reacduino__cos_out *out) ;

void Reacduino__sin_step(float x,
			 Reacduino__sin_out *out) ;

void Reacduino__tan_step(float x,
			 Reacduino__tan_out *out) ;
#endif
