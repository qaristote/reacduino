#ifndef REACDUINO_MATH_TYPES_H
#define REACDUINO_MATH_TYPES_H

#include "../reacduino_types.h"

/* Integer math */

typedef int_out Reacduino__intOfFloat_out ;

typedef int_out Reacduino__absInt_out ;

typedef int_out Reacduino__constrainInt_out ;

typedef int_out Reacduino__mapInt_out ;

typedef int_out Reacduino__maxInt_out ;

typedef int_out Reacduino__minInt_out ;

typedef int_out Reacduino__powInt_out ;

typedef int_out Reacduino__sqInt_out ;


/* Floating-point math */

typedef float_out Reacduino__floatOfInt_out ;

typedef float_out Reacduino__absFloat_out ;

typedef float_out Reacduino__constrainFloat_out ;

typedef float_out Reacduino__mapFloat_out ;

typedef float_out Reacduino__maxFloat_out ;

typedef float_out Reacduino__minFloat_out ;

typedef float_out Reacduino__powFloat_out ;

typedef float_out Reacduino__sqFloat_out ;

typedef float_out Reacduino__sqrt_out ;

typedef float_out Reacduino__cos_out ;

typedef float_out Reacduino__sin_out ;

typedef float_out Reacduino__tan_out ;

#endif
