#include "reacduino_pins.h"
#include "../macros/reacduino_macros.h"
  
int getGPIOPinNumber(Reacduino__gpio_pin pin) {
  switch (pin) {
#if digitalPinIsAvailable(0)
  case Reacduino__GPIO_D0 :
    return 0 ;
#endif
#if digitalPinIsAvailable(1)
  case Reacduino__GPIO_D1 :
    return 1 ;
#endif
#if digitalPinIsAvailable(2)
  case Reacduino__GPIO_D2 :
    return 2 ;
#endif
#if digitalPinIsAvailable(3)
  case Reacduino__GPIO_D3 :
    return 3 ;
#endif
#if digitalPinIsAvailable(4)
  case Reacduino__GPIO_D4 :
    return 4 ;
#endif
#if digitalPinIsAvailable(5)
  case Reacduino__GPIO_D5 :
    return 5 ;
#endif
#if digitalPinIsAvailable(6)
  case Reacduino__GPIO_D6 :
    return 6 ;
#endif
#if digitalPinIsAvailable(7)
  case Reacduino__GPIO_D7 :
    return 7 ;
#endif
#if digitalPinIsAvailable(8)
  case Reacduino__GPIO_D8 :
    return 8 ;
#endif
#if digitalPinIsAvailable(9)
  case Reacduino__GPIO_D9 :
    return 9 ;
#endif
#if digitalPinIsAvailable(10)
  case Reacduino__GPIO_D10 :
    return 10 ;
#endif
#if digitalPinIsAvailable(11)
  case Reacduino__GPIO_D11 :
    return 11 ;
#endif
#if digitalPinIsAvailable(12)
  case Reacduino__GPIO_D12 :
    return 12 ;
#endif
#if digitalPinIsAvailable(13)
  case Reacduino__GPIO_D13 :
    return 13 ;
#endif
#if digitalPinIsAvailable(14)
  case Reacduino__GPIO_D14 :
    return 14 ;
#endif
#if digitalPinIsAvailable(15)
  case Reacduino__GPIO_D15 :
    return 15 ;
#endif
#if digitalPinIsAvailable(16)
  case Reacduino__GPIO_D16 :
    return 16 ;
#endif
#if digitalPinIsAvailable(17)
  case Reacduino__GPIO_D17 :
    return 17 ;
#endif
#if digitalPinIsAvailable(18)
  case Reacduino__GPIO_D18 :
    return 18 ;
#endif
#if digitalPinIsAvailable(19)
  case Reacduino__GPIO_D19 :
    return 19 ;
#endif
#if digitalPinIsAvailable(20)
  case Reacduino__GPIO_D20 :
    return 20 ;
#endif
#if digitalPinIsAvailable(21)
  case Reacduino__GPIO_D21 :
    return 21 ;
#endif
#if digitalPinIsAvailable(22)
  case Reacduino__GPIO_D22 :
    return 22 ;
#endif
#if digitalPinIsAvailable(23)
  case Reacduino__GPIO_D23 :
    return 23 ;
#endif
#if digitalPinIsAvailable(24)
  case Reacduino__GPIO_D24 :
    return 24 ;
#endif
#if digitalPinIsAvailable(25)
  case Reacduino__GPIO_D25 :
    return 25 ;
#endif
#if digitalPinIsAvailable(26)
  case Reacduino__GPIO_D26 :
    return 26 ;
#endif
#if digitalPinIsAvailable(27)
  case Reacduino__GPIO_D27 :
    return 27 ;
#endif
#if digitalPinIsAvailable(28)
  case Reacduino__GPIO_D28 :
    return 28 ;
#endif
#if digitalPinIsAvailable(29)
  case Reacduino__GPIO_D29 :
    return 29 ;
#endif
#if digitalPinIsAvailable(30)
  case Reacduino__GPIO_D30 :
    return 30 ;
#endif
#if digitalPinIsAvailable(31)
  case Reacduino__GPIO_D31 :
    return 31 ;
#endif
#if digitalPinIsAvailable(32)
  case Reacduino__GPIO_D32 :
    return 32 ;
#endif
#if digitalPinIsAvailable(33)
  case Reacduino__GPIO_D33 :
    return 33 ;
#endif
#if digitalPinIsAvailable(34)
  case Reacduino__GPIO_D34 :
    return 34 ;
#endif
#if digitalPinIsAvailable(35)
  case Reacduino__GPIO_D35 :
    return 35 ;
#endif
#if digitalPinIsAvailable(36)
  case Reacduino__GPIO_D36 :
    return 36 ;
#endif
#if digitalPinIsAvailable(37)
  case Reacduino__GPIO_D37 :
    return 37 ;
#endif
#if digitalPinIsAvailable(38)
  case Reacduino__GPIO_D38 :
    return 38 ;
#endif
#if digitalPinIsAvailable(39)
  case Reacduino__GPIO_D39 :
    return 39 ;
#endif
#if digitalPinIsAvailable(40)
  case Reacduino__GPIO_D40 :
    return 40 ;
#endif
#if digitalPinIsAvailable(41)
  case Reacduino__GPIO_D41 :
    return 41 ;
#endif
#if digitalPinIsAvailable(42)
  case Reacduino__GPIO_D42 :
    return 42 ;
#endif
#if digitalPinIsAvailable(43)
  case Reacduino__GPIO_D43 :
    return 43 ;
#endif
#if digitalPinIsAvailable(44)
  case Reacduino__GPIO_D44 :
    return 44 ;
#endif
#if digitalPinIsAvailable(45)
  case Reacduino__GPIO_D45 :
    return 45 ;
#endif
#if digitalPinIsAvailable(46)
  case Reacduino__GPIO_D46 :
    return 46 ;
#endif
#if digitalPinIsAvailable(47)
  case Reacduino__GPIO_D47 :
    return 47 ;
#endif
#if digitalPinIsAvailable(48)
  case Reacduino__GPIO_D48 :
    return 48 ;
#endif
#if digitalPinIsAvailable(49)
  case Reacduino__GPIO_D49 :
    return 49 ;
#endif
#if digitalPinIsAvailable(50)
  case Reacduino__GPIO_D50 :
    return 50 ;
#endif
#if digitalPinIsAvailable(51)
  case Reacduino__GPIO_D51 :
    return 51 ;
#endif
#if digitalPinIsAvailable(52)
  case Reacduino__GPIO_D52 :
    return 52 ;
#endif
#if digitalPinIsAvailable(53)
  case Reacduino__GPIO_D53 :
    return 53 ;
#endif
#if analogInputIsAvailable(0)
  case Reacduino__GPIO_A0 :
    return A0 ;
#endif
#if analogInputIsAvailable(1)
  case Reacduino__GPIO_A1 :
    return A1 ;
#endif
#if analogInputIsAvailable(2)
  case Reacduino__GPIO_A2 :
    return A2 ;
#endif
#if analogInputIsAvailable(3)
  case Reacduino__GPIO_A3 :
    return A3 ;
#endif
#if analogInputIsAvailable(4)
  case Reacduino__GPIO_A4 :
    return A4 ;
#endif
#if analogInputIsAvailable(5)
  case Reacduino__GPIO_A5 :
    return A5 ;
#endif
#if analogInputIsAvailable(6)
  case Reacduino__GPIO_A6 :
    return A6 ;
#endif
#if analogInputIsAvailable(7)
  case Reacduino__GPIO_A7 :
    return A7 ;
#endif
#if analogInputIsAvailable(8)
  case Reacduino__GPIO_A8 :
    return A8 ;
#endif
#if analogInputIsAvailable(9)
  case Reacduino__GPIO_A9 :
    return A9 ;
#endif
#if analogInputIsAvailable(10)
  case Reacduino__GPIO_A10 :
    return A10 ;
#endif
#if analogInputIsAvailable(11)
  case Reacduino__GPIO_A11 :
    return A11 ;
#endif
#if analogInputIsAvailable(12)
  case Reacduino__GPIO_A12 :
    return A12 ;
#endif
#if analogInputIsAvailable(13)
  case Reacduino__GPIO_A13 :
    return A13 ;
#endif
#if analogInputIsAvailable(14)
  case Reacduino__GPIO_A14 :
    return A14 ;
#endif
#if analogInputIsAvailable(15)
  case Reacduino__GPIO_A15 :
    return A15 ;
#endif    
  } ;
}

int getAnalogPinNumber(Reacduino__analog_pin pin) {
  switch (pin) {
#if analogInputIsAvailable(0)
  case Reacduino__A0 :
    return A0 ;
#endif
#if analogInputIsAvailable(1)
  case Reacduino__A1 :
    return A1 ;
#endif
#if analogInputIsAvailable(2)
  case Reacduino__A2 :
    return A2 ;
#endif
#if analogInputIsAvailable(3)
  case Reacduino__A3 :
    return A3 ;
#endif
#if analogInputIsAvailable(4)
  case Reacduino__A4 :
    return A4 ;
#endif
#if analogInputIsAvailable(5)
  case Reacduino__A5 :
    return A5 ;
#endif
#if analogInputIsAvailable(6)
  case Reacduino__A6 :
    return A6 ;
#endif
#if analogInputIsAvailable(7)
  case Reacduino__A7 :
    return A7 ;
#endif
#if analogInputIsAvailable(8)
  case Reacduino__A8 :
    return A8 ;
#endif
#if analogInputIsAvailable(9)
  case Reacduino__A9 :
    return A9 ;
#endif
#if analogInputIsAvailable(10)
  case Reacduino__A10 :
    return A10 ;
#endif
#if analogInputIsAvailable(11)
  case Reacduino__A11 :
    return A11 ;
#endif
#if analogInputIsAvailable(12)
  case Reacduino__A12 :
    return A12 ;
#endif
#if analogInputIsAvailable(13)
  case Reacduino__A13 :
    return A13 ;
#endif
#if analogInputIsAvailable(14)
  case Reacduino__A14 :
    return A14 ;
#endif
#if analogInputIsAvailable(15)
  case Reacduino__A15 :
    return A15 ;
#endif
  } ;
}

int getPWMPinNumber(Reacduino__pwm_pin pin) {
  switch (pin) {
#if (digitalPinHasPWM(0))
  case Reacduino__PWM_D0 :
    return 0 ;
#endif
#if (digitalPinHasPWM(1))
  case Reacduino__PWM_D1 :
    return 1 ;
#endif
#if (digitalPinHasPWM(2))
  case Reacduino__PWM_D2 :
    return 2 ;
#endif
#if (digitalPinHasPWM(3))
  case Reacduino__PWM_D3 :
    return 3 ;
#endif
#if (digitalPinHasPWM(4))
  case Reacduino__PWM_D4 :
    return 4 ;
#endif
#if (digitalPinHasPWM(5))
  case Reacduino__PWM_D5 :
    return 5 ;
#endif
#if (digitalPinHasPWM(6))
  case Reacduino__PWM_D6 :
    return 6 ;
#endif
#if (digitalPinHasPWM(7))
  case Reacduino__PWM_D7 :
    return 7 ;
#endif
#if (digitalPinHasPWM(8))
  case Reacduino__PWM_D8 :
    return 8 ;
#endif
#if (digitalPinHasPWM(9))
  case Reacduino__PWM_D9 :
    return 9 ;
#endif
#if (digitalPinHasPWM(10))
  case Reacduino__PWM_D10 :
    return 10 ;
#endif
#if (digitalPinHasPWM(11))
  case Reacduino__PWM_D11 :
    return 11 ;
#endif
#if (digitalPinHasPWM(12))
  case Reacduino__PWM_D12 :
    return 12 ;
#endif
#if (digitalPinHasPWM(13))
  case Reacduino__PWM_D13 :
    return 13 ;
#endif
#if (digitalPinHasPWM(44))
  case Reacduino__PWM_D44 :
    return 44 ;
#endif
#if (digitalPinHasPWM(45))
  case Reacduino__PWM_D45 :
    return 45 ;
#endif
#if (digitalPinHasPWM(46))
  case Reacduino__PWM_D46 :
    return 46 ;
#endif
#if analogInputHasPWM(0)
  case Reacduino__PWM_A0 :
    return A0 ;
#endif
#if analogInputHasPWM(1)
  case Reacduino__PWM_A1 :
    return A1 ;
#endif
#if analogInputHasPWM(2)
  case Reacduino__PWM_A2 :
    return A2 ;
#endif
#if analogInputHasPWM(3)
  case Reacduino__PWM_A3 :
    return A3 ;
#endif
#if analogInputHasPWM(4)
  case Reacduino__PWM_A4 :
    return A4 ;
#endif
#if analogInputHasPWM(5)
  case Reacduino__PWM_A5 :
    return A5 ;
#endif
#if analogInputHasPWM(6)
  case Reacduino__PWM_A6 :
    return A6 ;
#endif
#if analogInputHasPWM(7)
  case Reacduino__PWM_A7 :
    return A7 ;
#endif
  } ;
}
