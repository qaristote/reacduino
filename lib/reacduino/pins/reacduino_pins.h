#ifndef REACDUINO_PINS_H
#define REACDUINO_PINS_H

#include "reacduino_pins_types.h"

int getGPIOPinNumber(Reacduino__gpio_pin pin) ;
int getAnalogPinNumber(Reacduino__analog_pin pin) ;
int getPWMPinNumber(Reacduino__pwm_pin pin) ;
int getISRPinNumber(Reacduino__pwm_pin pin) ;

#endif
