#ifndef REACDUINO_PINS_TYPES_H
#define REACDUINO_PINS_TYPES_H

#include "macros/reacduino_macros.h"

typedef enum {
#if analogInputIsAvailable(0)
	      Reacduino__A0,
#endif
#if analogInputIsAvailable(1)
	      Reacduino__A1,
#endif
#if analogInputIsAvailable(2)
	      Reacduino__A2,
#endif
#if analogInputIsAvailable(3)
	      Reacduino__A3,
#endif
#if analogInputIsAvailable(4)
	      Reacduino__A4,
#endif
#if analogInputIsAvailable(5)
	      Reacduino__A5,
#endif
#if analogInputIsAvailable(6)
	      Reacduino__A6,
#endif
#if analogInputIsAvailable(7)
	      Reacduino__A7,
#endif
#if analogInputIsAvailable(8)
	      Reacduino__A8,
#endif
#if analogInputIsAvailable(9)
	      Reacduino__A9,
#endif
#if analogInputIsAvailable(10)
	      Reacduino__A10,
#endif
#if analogInputIsAvailable(11)
	      Reacduino__A11,
#endif
#if analogInputIsAvailable(12)
	      Reacduino__A12,
#endif
#if analogInputIsAvailable(13)
	      Reacduino__A13,
#endif
#if analogInputIsAvailable(14)
	      Reacduino__A14,
#endif
#if analogInputIsAvailable(15)
	      Reacduino__A15
#endif
} Reacduino__analog_pin ;

typedef enum {
#if digitalPinHasPWM(0)
	      Reacduino__PWM_D0,
#endif	      
#if digitalPinHasPWM(1)
	      Reacduino__PWM_D1,
#endif	      
#if digitalPinHasPWM(2)
	      Reacduino__PWM_D2,
#endif	      
#if digitalPinHasPWM(3)
	      Reacduino__PWM_D3,
#endif	      
#if digitalPinHasPWM(4)
	      Reacduino__PWM_D4,
#endif	      
#if digitalPinHasPWM(5)
	      Reacduino__PWM_D5,
#endif	      
#if digitalPinHasPWM(6)
	      Reacduino__PWM_D6,
#endif	      
#if digitalPinHasPWM(7)
	      Reacduino__PWM_D7,
#endif	      
#if digitalPinHasPWM(8)
	      Reacduino__PWM_D8,
#endif	      
#if digitalPinHasPWM(9)
	      Reacduino__PWM_D9,
#endif	      
#if digitalPinHasPWM(10)
	      Reacduino__PWM_D10,
#endif	      
#if digitalPinHasPWM(11)
	      Reacduino__PWM_D11,
#endif	      
#if digitalPinHasPWM(12)
	      Reacduino__PWM_D12,
#endif	      
#if digitalPinHasPWM(13)
	      Reacduino__PWM_D13,
#endif	      
#if digitalPinHasPWM(44)
	      Reacduino__PWM_D44,
#endif	      
#if digitalPinHasPWM(45)
	      Reacduino__PWM_D45,
#endif	      
#if digitalPinHasPWM(46)
	      Reacduino__PWM_D46,
#endif	      
#if analogInputHasPWM(0)
	      Reacduino__PWM_A0,
#endif
#if analogInputHasPWM(1)
	      Reacduino__PWM_A1,
#endif
#if analogInputHasPWM(2)
	      Reacduino__PWM_A2,
#endif
#if analogInputHasPWM(3)
	      Reacduino__PWM_A3,
#endif
#if analogInputHasPWM(4)
	      Reacduino__PWM_A4,
#endif
#if analogInputHasPWM(5)
	      Reacduino__PWM_A5,
#endif
#if analogInputHasPWM(6)
	      Reacduino__PWM_A6,
#endif
#if analogInputHasPWM(7)
	      Reacduino__PWM_A7
#endif
} Reacduino__pwm_pin ;

typedef enum {
#if digitalPinIsAvailable(0)
	      Reacduino__GPIO_D0,
#endif
#if digitalPinIsAvailable(1)
	      Reacduino__GPIO_D1,
#endif
#if digitalPinIsAvailable(2)
	      Reacduino__GPIO_D2,
#endif
#if digitalPinIsAvailable(3)
	      Reacduino__GPIO_D3,
#endif
#if digitalPinIsAvailable(4)
	      Reacduino__GPIO_D4,
#endif
#if digitalPinIsAvailable(5)
	      Reacduino__GPIO_D5,
#endif
#if digitalPinIsAvailable(6)
	      Reacduino__GPIO_D6,
#endif
#if digitalPinIsAvailable(7)
	      Reacduino__GPIO_D7,
#endif
#if digitalPinIsAvailable(8)
	      Reacduino__GPIO_D8,
#endif
#if digitalPinIsAvailable(9)
	      Reacduino__GPIO_D9,
#endif
#if digitalPinIsAvailable(10)
	      Reacduino__GPIO_D10,
#endif
#if digitalPinIsAvailable(11)
	      Reacduino__GPIO_D11,
#endif
#if digitalPinIsAvailable(12)
	      Reacduino__GPIO_D12,
#endif
#if digitalPinIsAvailable(13)
	      Reacduino__GPIO_D13,
#endif
#if digitalPinIsAvailable(14)
	      Reacduino__GPIO_D14,
#endif
#if digitalPinIsAvailable(15)
	      Reacduino__GPIO_D15,
#endif
#if digitalPinIsAvailable(16)
	      Reacduino__GPIO_D16,
#endif
#if digitalPinIsAvailable(17)
	      Reacduino__GPIO_D17,
#endif
#if digitalPinIsAvailable(18)
	      Reacduino__GPIO_D18,
#endif
#if digitalPinIsAvailable(19)
	      Reacduino__GPIO_D19,
#endif
#if digitalPinIsAvailable(20)
	      Reacduino__GPIO_D20,
#endif
#if digitalPinIsAvailable(21)
	      Reacduino__GPIO_D21,
#endif
#if digitalPinIsAvailable(22)
	      Reacduino__GPIO_D22,
#endif
#if digitalPinIsAvailable(23)
	      Reacduino__GPIO_D23,
#endif
#if digitalPinIsAvailable(24)
	      Reacduino__GPIO_D24,
#endif
#if digitalPinIsAvailable(25)
	      Reacduino__GPIO_D25,
#endif
#if digitalPinIsAvailable(26)
	      Reacduino__GPIO_D26,
#endif
#if digitalPinIsAvailable(27)
	      Reacduino__GPIO_D27,
#endif
#if digitalPinIsAvailable(28)
	      Reacduino__GPIO_D28,
#endif
#if digitalPinIsAvailable(29)
	      Reacduino__GPIO_D29,
#endif
#if digitalPinIsAvailable(30)
	      Reacduino__GPIO_D30,
#endif
#if digitalPinIsAvailable(31)
	      Reacduino__GPIO_D31,
#endif
#if digitalPinIsAvailable(32)
	      Reacduino__GPIO_D32,
#endif
#if digitalPinIsAvailable(33)
	      Reacduino__GPIO_D33,
#endif
#if digitalPinIsAvailable(34)
	      Reacduino__GPIO_D34,
#endif
#if digitalPinIsAvailable(35)
	      Reacduino__GPIO_D35,
#endif
#if digitalPinIsAvailable(36)
	      Reacduino__GPIO_D36,
#endif
#if digitalPinIsAvailable(37)
	      Reacduino__GPIO_D37,
#endif
#if digitalPinIsAvailable(38)
	      Reacduino__GPIO_D38,
#endif
#if digitalPinIsAvailable(39)
	      Reacduino__GPIO_D39,
#endif
#if digitalPinIsAvailable(40)
	      Reacduino__GPIO_D40,
#endif
#if digitalPinIsAvailable(41)
	      Reacduino__GPIO_D41,
#endif
#if digitalPinIsAvailable(42)
	      Reacduino__GPIO_D42,
#endif
#if digitalPinIsAvailable(43)
	      Reacduino__GPIO_D43,
#endif
#if digitalPinIsAvailable(44)
	      Reacduino__GPIO_D44,
#endif
#if digitalPinIsAvailable(45)
	      Reacduino__GPIO_D45,
#endif
#if digitalPinIsAvailable(46)
	      Reacduino__GPIO_D46,
#endif
#if digitalPinIsAvailable(47)
	      Reacduino__GPIO_D47,
#endif
#if digitalPinIsAvailable(48)
	      Reacduino__GPIO_D48,
#endif
#if digitalPinIsAvailable(49)
	      Reacduino__GPIO_D49,
#endif
#if digitalPinIsAvailable(50)
	      Reacduino__GPIO_D50,
#endif
#if digitalPinIsAvailable(51)
	      Reacduino__GPIO_D51,
#endif
#if digitalPinIsAvailable(52)
	      Reacduino__GPIO_D52,
#endif
#if digitalPinIsAvailable(53)
	      Reacduino__GPIO_D53,
#endif
#if analogInputIsAvailable(0)
	      Reacduino__GPIO_A0,
#endif
#if analogInputIsAvailable(1)
	      Reacduino__GPIO_A1,
#endif
#if analogInputIsAvailable(2)
	      Reacduino__GPIO_A2,
#endif
#if analogInputIsAvailable(3)
	      Reacduino__GPIO_A3,
#endif
#if analogInputIsAvailable(4)
	      Reacduino__GPIO_A4,
#endif
#if analogInputIsAvailable(5)
	      Reacduino__GPIO_A5,
#endif
#if analogInputIsAvailable(6)
	      Reacduino__GPIO_A6,
#endif
#if analogInputIsAvailable(7)
	      Reacduino__GPIO_A7,
#endif
#if analogInputIsAvailable(8)
	      Reacduino__GPIO_A8,
#endif
#if analogInputIsAvailable(9)
	      Reacduino__GPIO_A9,
#endif
#if analogInputIsAvailable(10)
	      Reacduino__GPIO_A10,
#endif
#if analogInputIsAvailable(11)
	      Reacduino__GPIO_A11,
#endif
#if analogInputIsAvailable(12)
	      Reacduino__GPIO_A12,
#endif
#if analogInputIsAvailable(13)
	      Reacduino__GPIO_A13,
#endif
#if analogInputIsAvailable(14)
	      Reacduino__GPIO_A14,
#endif
#if analogInputIsAvailable(15)
	      Reacduino__GPIO_A15
#endif	      
} Reacduino__gpio_pin ;

typedef enum {
#if digitalPinHasInterrupts(0)
	      Reacduino__ISR_D0,
#endif
#if digitalPinHasInterrupts(1)
	      Reacduino__ISR_D1,
#endif
#if digitalPinHasInterrupts(2)
	      Reacduino__ISR_D2,
#endif
#if digitalPinHasInterrupts(3)
	      Reacduino__ISR_D3,
#endif
#if digitalPinHasInterrupts(4)
	      Reacduino__ISR_D4,
#endif
#if digitalPinHasInterrupts(5)
	      Reacduino__ISR_D5,
#endif
#if digitalPinHasInterrupts(6)
	      Reacduino__ISR_D6,
#endif
#if digitalPinHasInterrupts(7)
	      Reacduino__ISR_D7,
#endif
#if digitalPinHasInterrupts(8)
	      Reacduino__ISR_D8,
#endif
#if digitalPinHasInterrupts(9)
	      Reacduino__ISR_D9,
#endif
#if digitalPinHasInterrupts(10)
	      Reacduino__ISR_D10,
#endif
#if digitalPinHasInterrupts(11)
	      Reacduino__ISR_D11,
#endif
#if digitalPinHasInterrupts(12)
	      Reacduino__ISR_D12,
#endif
#if digitalPinHasInterrupts(13)
	      Reacduino__ISR_D13,
#endif
#if digitalPinHasInterrupts(14)
	      Reacduino__ISR_D14,
#endif
#if digitalPinHasInterrupts(15)
	      Reacduino__ISR_D15,
#endif
#if digitalPinHasInterrupts(16)
	      Reacduino__ISR_D16,
#endif
#if digitalPinHasInterrupts(17)
	      Reacduino__ISR_D17,
#endif
#if digitalPinHasInterrupts(18)
	      Reacduino__ISR_D18,
#endif
#if digitalPinHasInterrupts(19)
	      Reacduino__ISR_D19,
#endif
#if digitalPinHasInterrupts(20)
	      Reacduino__ISR_D20,
#endif
#if digitalPinHasInterrupts(21)
	      Reacduino__ISR_D21,
#endif
#if digitalPinHasInterrupts(22)
	      Reacduino__ISR_D22,
#endif
#if digitalPinHasInterrupts(23)
	      Reacduino__ISR_D23,
#endif
#if digitalPinHasInterrupts(24)
	      Reacduino__ISR_D24,
#endif
#if digitalPinHasInterrupts(25)
	      Reacduino__ISR_D25,
#endif
#if digitalPinHasInterrupts(26)
	      Reacduino__ISR_D26,
#endif
#if digitalPinHasInterrupts(27)
	      Reacduino__ISR_D27,
#endif
#if digitalPinHasInterrupts(28)
	      Reacduino__ISR_D28,
#endif
#if digitalPinHasInterrupts(29)
	      Reacduino__ISR_D29,
#endif
#if digitalPinHasInterrupts(30)
	      Reacduino__ISR_D30,
#endif
#if digitalPinHasInterrupts(31)
	      Reacduino__ISR_D31,
#endif
#if digitalPinHasInterrupts(32)
	      Reacduino__ISR_D32,
#endif
#if digitalPinHasInterrupts(33)
	      Reacduino__ISR_D33,
#endif
#if digitalPinHasInterrupts(34)
	      Reacduino__ISR_D34,
#endif
#if digitalPinHasInterrupts(35)
	      Reacduino__ISR_D35,
#endif
#if digitalPinHasInterrupts(36)
	      Reacduino__ISR_D36,
#endif
#if digitalPinHasInterrupts(37)
	      Reacduino__ISR_D37,
#endif
#if digitalPinHasInterrupts(38)
	      Reacduino__ISR_D38,
#endif
#if digitalPinHasInterrupts(39)
	      Reacduino__ISR_D39,
#endif
#if digitalPinHasInterrupts(40)
	      Reacduino__ISR_D40,
#endif
#if digitalPinHasInterrupts(41)
	      Reacduino__ISR_D41,
#endif
#if digitalPinHasInterrupts(42)
	      Reacduino__ISR_D42,
#endif
#if digitalPinHasInterrupts(43)
	      Reacduino__ISR_D43,
#endif
#if digitalPinHasInterrupts(44)
	      Reacduino__ISR_D44,
#endif
#if digitalPinHasInterrupts(45)
	      Reacduino__ISR_D45,
#endif
#if digitalPinHasInterrupts(46)
	      Reacduino__ISR_D46,
#endif
#if digitalPinHasInterrupts(47)
	      Reacduino__ISR_D47,
#endif
#if digitalPinHasInterrupts(48)
	      Reacduino__ISR_D48,
#endif
#if digitalPinHasInterrupts(49)
	      Reacduino__ISR_D49,
#endif
#if digitalPinHasInterrupts(50)
	      Reacduino__ISR_D50,
#endif
#if digitalPinHasInterrupts(51)
	      Reacduino__ISR_D51,
#endif
#if digitalPinHasInterrupts(52)
	      Reacduino__ISR_D52,
#endif
#if digitalPinHasInterrupts(53)
	      Reacduino__ISR_D53,
#endif
#if analogInputHasInterrupts(A0)
	      Reacduino__ISR_A0,
#endif
#if analogInputHasInterrupts(A1)
	      Reacduino__ISR_A1,
#endif
#if analogInputHasInterrupts(A2)
	      Reacduino__ISR_A2,
#endif
#if analogInputHasInterrupts(A3)
	      Reacduino__ISR_A3,
#endif
#if analogInputHasInterrupts(A4)
	      Reacduino__ISR_A4,
#endif
#if analogInputHasInterrupts(A5)
	      Reacduino__ISR_A5,
#endif
#if analogInputHasInterrupts(A6)
	      Reacduino__ISR_A6,
#endif
#if analogInputHasInterrupts(A7)
	      Reacduino__ISR_A7,
#endif
#if analogInputHasInterrupts(A8)
	      Reacduino__ISR_A8,
#endif
#if analogInputHasInterrupts(A9)
	      Reacduino__ISR_A9,
#endif
#if analogInputHasInterrupts(A10)
	      Reacduino__ISR_A10,
#endif
#if analogInputHasInterrupts(A11)
	      Reacduino__ISR_A11,
#endif
#if analogInputHasInterrupts(A12)
	      Reacduino__ISR_A12,
#endif
#if analogInputHasInterrupts(A13)
	      Reacduino__ISR_A13,
#endif
#if analogInputHasInterrupts(A14)
	      Reacduino__ISR_A14,
#endif
#if analogInputHasInterrupts(A15)
	      Reacduino__ISR_A15,
#endif				 
} Reacduino__isr_pin ;

#endif
