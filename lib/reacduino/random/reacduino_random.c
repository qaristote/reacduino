#include "Arduino.h"
#include "reacduino_random.h"

void Reacduino__randInt_step(int min, int max,
			     Reacduino__randInt_out *out) {
  out->value = (int) (min + random() * (max + 1 - min)) ;
}

void Reacduino__randFloat_step(float min, float max,
			       Reacduino__randFloat_out *out) {
  out->value = (float) (min + random() * (max - min)) ;
}
