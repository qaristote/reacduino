#ifndef REACDUINO_RANDOM_H
#define REACDUINO_RANDOM_H

#include "reacduino_random_types.h"

void Reacduino__randInt_step(int min, int max,
			     Reacduino__randInt_out *out) ;

void Reacduino__randFloat_step(float min, float max,
			       Reacduino__randFloat_out *out) ;

#endif
