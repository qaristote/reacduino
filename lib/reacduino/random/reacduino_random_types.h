#ifndef REACDUINO_RANDOM_TYPES_H
#define REACDUINO_RANDOM_TYPES H

#include "../reacduino_types.h"

typedef int_out Reacduino__randInt_out ;

typedef float_out Reacduino__randFloat_out ;

#endif
