#ifndef REACDUINO_TYPES_H
#define REACDUINO_TYPES_H

#include <stdbool.h>
#include "pins/reacduino_pins_types.h"

/* Mem types */

typedef struct empty_mem {
} empty_mem ;


/* Out types */

typedef struct unit_out {
} unit_out ;

typedef struct bool_out {
  bool value ;
} bool_out ;

typedef struct int_out {
  int value ;
} int_out ;

typedef struct float_out {
  float value ;
} float_out ;

typedef struct byte_out {
  int value ;
} byte_out ;

#endif


