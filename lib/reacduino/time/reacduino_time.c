#include <stdbool.h>
#include "Arduino.h"
#include "reacduino_time.h"

void Reacduino__micros_step(Reacduino__micros_out *out) {
  out->value = (int) micros() ;
}

void Reacduino__millis_step(Reacduino__millis_out *out) {
  out->value = (int) millis() ;
}

void Reacduino__delayTicks_reset(Reacduino__delayTicks_mem *self) {
  self->ticks_elapsed = 0 ;
}
void Reacduino__delayTicks_step(int ticks,
				Reacduino__delayTicks_out *out,
				Reacduino__delayTicks_mem *self) {
  if (self->ticks_elapsed >= ticks) {
    out->value= true ;
  } else {
    out->value = false ;
  } ;
}

void Reacduino__delay_reset(Reacduino__delay_mem *self) {
  self->time_next = 0 ;
}
void Reacduino__delay_step(int duration,
			   Reacduino__delay_out *out,
			   Reacduino__delay_mem *self) {
  if (millis() >= self->time_next) {
    self->time_next = self->time_next + (unsigned long) duration ;
    out->value = true ;
  } else {
    out->value = false ;
  } ;
}

void Reacduino__delayMicroseconds_reset(Reacduino__delayMicroseconds_mem *self) {
  self->time_next = 0 ;
}
void Reacduino__delayMicroseconds_step(int duration,
				       Reacduino__delayMicroseconds_out *out,
				       Reacduino__delayMicroseconds_mem *self) {
  if (micros() >= self->time_next) {
    self->time_next = self->time_next + (unsigned long) duration ;
    out->value = true ;
  } else {
    out->value = false ;
  } ;
}

void Reacduino__delayOnce_reset(Reacduino__delayOnce_mem *self) {
  self->time_start = millis() ;
  self->clock = false ;
}
void Reacduino__delayOnce_step(int duration,
			       Reacduino__delayOnce_out *out,
			       Reacduino__delayOnce_mem *self) {
  if (!(self->clock)) {
    self->clock = millis() >= self->time_start + (unsigned long) duration ;
  } ;
  out->value = self->clock ;
}

void Reacduino__delayOnceMicroseconds_reset(Reacduino__delayOnceMicroseconds_mem *self) {
    self->time_start = micros() ;
    self->clock = false ;
}
void Reacduino__delayOnceMicroseconds_step(int duration,
					   Reacduino__delayOnceMicroseconds_out *out,
					   Reacduino__delayOnceMicroseconds_mem *self) {
  if (!(self->clock)) {
    self->clock = micros() >= self->time_start + (unsigned long) duration ;
  } ;
  out->value = self->clock ;
}
