#ifndef REACDUINO_TIME_H
#define REACDUINO_TIME_H

#include "reacduino_time_types.h"

void Reacduino__micros_step(Reacduino__micros_out *out) ;

void Reacduino__millis_step(Reacduino__millis_out *out) ;

void Reacduino__delayTicks_reset(Reacduino__delayTicks_mem *self) ;
void Reacduino__delayTicks_step(int ticks,
				Reacduino__delayTicks_out *out,
				Reacduino__delayTicks_mem *self) ;

void Reacduino__delay_reset(Reacduino__delay_mem *self) ;
void Reacduino__delay_step(int duration,
			   Reacduino__delay_out *out,
			   Reacduino__delay_mem *self) ;

void Reacduino__delayMicroseconds_reset(Reacduino__delayMicroseconds_mem *self) ;
void Reacduino__delayMicroseconds_step(int duration,
				       Reacduino__delayMicroseconds_out *out,
				       Reacduino__delayMicroseconds_mem *self) ;

void Reacduino__delayOnce_reset(Reacduino__delayOnce_mem *self) ;
void Reacduino__delayOnce_step(int duration,
			       Reacduino__delayOnce_out *out,
			       Reacduino__delayOnce_mem *self) ;

void Reacduino__delayOnceMicroseconds_reset(Reacduino__delayOnceMicroseconds_mem *self) ;
void Reacduino__delayOnceMicroseconds_step(int duration,
					   Reacduino__delayOnceMicroseconds_out *out,
					   Reacduino__delayOnceMicroseconds_mem *self) ;
#endif
