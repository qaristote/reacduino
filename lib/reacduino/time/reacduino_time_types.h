#ifndef REACDUINO_TIME_TYPES_H
#define REACDUINO_TIME_TYPES_H

#include "../reacduino_types.h"

typedef int_out Reacduino__micros_out ;

typedef int_out Reacduino__millis_out ;

typedef struct Reacduino__delayTicks_mem {
  unsigned int ticks_elapsed ;
} Reacduino__delayTicks_mem ;
typedef bool_out Reacduino__delayTicks_out ;

typedef struct Reacduino__delay_mem {
  unsigned long time_next ;
} Reacduino__delay_mem ;
typedef bool_out Reacduino__delay_out ;

typedef struct Reacduino__delayMicroseconds_mem {
  unsigned long time_next ;
} Reacduino__delayMicroseconds_mem ;
typedef bool_out Reacduino__delayMicroseconds_out ;

typedef struct Reacduino__delayOnce_mem {
  unsigned long time_start ;
  bool clock ;
} Reacduino__delayOnce_mem ;
typedef bool_out Reacduino__delayOnce_out ;

typedef struct Reacduino__delayOnceMicroseconds_mem {
  unsigned long time_start ;
  bool clock ;
} Reacduino__delayOnceMicroseconds_mem ;
typedef bool_out Reacduino__delayOnceMicroseconds_out ;

#endif
